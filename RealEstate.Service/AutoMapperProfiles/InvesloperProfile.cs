﻿using AutoMapper;
using RealEstate.Core.Dtos.Invesloper;
using RealEstate.Core.Dtos.Table;
using RealEstate.Data.Entities;

namespace RealEstate.Services.AutoMapperProfiles
{
	public class InvesloperProfile : Profile
	{
		public InvesloperProfile()
		{
			CreateMap<InvesloperTableDataDto, Invesloper>().ReverseMap();
			CreateMap<InvesloperDetailsDto, Invesloper>().ReverseMap();
			CreateMap<TableData<InvesloperDetailsDto>, TableData<Invesloper>>().ReverseMap();
		}
	}
}
