﻿using AutoMapper;
using RealEstate.Core.Dtos.Project;
using RealEstate.Core.Extensions;
using RealEstate.Data.Entities;
using System;

namespace RealEstate.Services.AutoMapperProfiles
{
	public class ProjectProfile : Profile
	{
		public ProjectProfile()
		{
			CreateMap<ProjectDto, Project>()
				.ReverseMap()
				.ForMember(x => x.StartDate, y => y.MapFrom((projEntity, projectDto) => {
					return DateTime.SpecifyKind(projEntity.StartDate, DateTimeKind.Utc);
				}));

			CreateMap<Project, ProjectTableDataDto>()
				.ForMember(x => x.InvesloperName, y => y.MapFrom(t => t.Invesloper.Name))
				.ForMember(x => x.Type, y => y.MapFrom(x => x.Type.Description()));

            CreateMap<Project, HomePageProjectDto>();

            CreateMap<Project, ProjectClientDto>();
        }
	}
}
