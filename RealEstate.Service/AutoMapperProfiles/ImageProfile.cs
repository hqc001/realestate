﻿using AutoMapper;
using RealEstate.Core.Dtos.Image;
using RealEstate.Data.Entities;

namespace RealEstate.Services.AutoMapperProfiles
{
    public class ImageProfile: Profile
    {
        public ImageProfile()
        {
            CreateMap<Image, ImageTableDataDto>()
                .ForMember(x => x.ProjectName, y => y.MapFrom(t => t.Project.Name));

            CreateMap<Image, ImageDto>().ReverseMap();
        }
    }
}
