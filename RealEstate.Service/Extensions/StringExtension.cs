﻿using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace RealEstate.Services.Extensions
{
	public static class StringExtension
	{
		public static string ConvertToUnsign(this string str)
		{
			Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
			string temp = str.Normalize(NormalizationForm.FormD);
			return regex.Replace(temp, string.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
		}

		public static string ToDashCase(this string str)
		{
			return string.Join('-', str.ConvertToUnsign().Split(' ').Where(x => !string.IsNullOrWhiteSpace(x))).ToLowerInvariant();
		}
	}
}
