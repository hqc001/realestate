﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using RealEstate.Core.Contracts.Services;
using RealEstate.Services.Services;
using System.Reflection;

namespace RealEstate.Services.Extensions
{
	public static class ServicesModuleServiceCollectionExtension
	{
		public static IServiceCollection AddServicesModule(this IServiceCollection services)
		{
			services.AddAutoMapper(config => {
				config.AddProfiles(Assembly.GetExecutingAssembly());
			});

			services.AddTransient<IInvesloperService, InvesloperService>();
            services.AddTransient<IProjectService, ProjectService>();
            services.AddTransient<IImageService, ImageService>();

            return services;
		}
	}
}
