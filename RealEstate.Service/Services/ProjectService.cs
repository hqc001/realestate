﻿using AutoMapper;
using RealEstate.Core.Constants.Enums;
using RealEstate.Core.Contracts.Services;
using RealEstate.Core.Dtos.Commons;
using RealEstate.Core.Dtos.Project;
using RealEstate.Core.Dtos.Table;
using RealEstate.Core.Extensions;
using RealEstate.Data.DBContext;
using RealEstate.Data.Entities;
using RealEstate.Data.Repositories.Contracts;
using RealEstate.Services.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace RealEstate.Services.Services
{
    public class ProjectService : IProjectService
    {
        private readonly ITableRepository<Project> _projectRepo;
        private readonly IRepository<Invesloper> _invesloperRepo;
        private readonly RealEstateDbContext _realEstateDbContext;
        private readonly IMapper _mapper;

        public ProjectService(
            ITableRepository<Project> projectRepo,
            IRepository<Invesloper> invesloperRepo,
            RealEstateDbContext realEstateDbContext,
            IMapper mapper
        )
        {
            _projectRepo = projectRepo;
            _invesloperRepo = invesloperRepo;
            _realEstateDbContext = realEstateDbContext;
            _mapper = mapper;
        }

        public async Task<SimpleSelectOptionDto> GetProjectsAsync()
        {
            var projectSelectOpts = new List<SelectOptionDto<int>>();

            var result = new SimpleSelectOptionDto
            {
                Items = projectSelectOpts
            };

            var projects = await _projectRepo.GetAllAsync().ToList();
            projects.ForEach(AddProjectSelectOpts);

            void AddProjectSelectOpts(Project project) => projectSelectOpts.Add(new SelectOptionDto<int>
            {
                Value = project.Id,
                Description = project.Name
            });

            return result;
        }

        public async Task<ProjectFormDto> GetProjectFormData(int id)
        {
            var invesloperSelectOpts = new List<SelectOptionDto<int>>();
            var projStatusSelectOpts = new List<SelectOptionDto<ProjectStatuses>>();
            var projTypesSelectOpts = new List<SelectOptionDto<ProjectTypes>>();

            var result = new ProjectFormDto
            {
                ProjectInveslopers = invesloperSelectOpts,
                ProjectStatuses = projStatusSelectOpts,
                ProjectTypes = projTypesSelectOpts
            };

            var inveslopers = await _invesloperRepo.GetAllAsync().ToList();
            inveslopers.ForEach(AddInvesloperSelectOpts);

            projStatusSelectOpts.AddRange(new SelectOptionDto<ProjectStatuses>[3] {
                new SelectOptionDto<ProjectStatuses> { Value = ProjectStatuses.Opened, Description = ProjectStatuses.Opened.Description() },
                new SelectOptionDto<ProjectStatuses> { Value = ProjectStatuses.Closed, Description = ProjectStatuses.Closed.Description() },
                new SelectOptionDto<ProjectStatuses> { Value = ProjectStatuses.ComingSoon, Description = ProjectStatuses.ComingSoon.Description() }
            });

            projTypesSelectOpts.AddRange(new SelectOptionDto<ProjectTypes>[1] {
                new SelectOptionDto<ProjectTypes> { Value = ProjectTypes.Housing, Description = ProjectTypes.Housing.Description() }
            });

            if (id > 0)
            {
                var project = await _projectRepo.GetByIdAsync(id, (include) =>
                {
                    include.Add(x => x.DisplayImage);
                    include.Add(x => x.Images);
                });

                if (project != null)
                {
                    result.Project = _mapper.Map<ProjectDto>(project);
                    result.ProjectImages = project.Images.Select(x => new SelectOptionDto<int>
                    {
                        Description = x.ThumbnailName,
                        Value = x.Id
                    });
                }
            }

            return result;

            void AddInvesloperSelectOpts(Invesloper inves) => invesloperSelectOpts.Add(new SelectOptionDto<int>
            {
                Value = inves.Id,
                Description = inves.Name
            });
        }

        public async Task<ProjectCreateUpdateResultDto> CreateOrUpdateProject(ProjectDto projectDto)
        {
            projectDto.Uid = projectDto.Name.ToDashCase();

            if (_projectRepo.IsExist(x => x.Id != projectDto.Id && (x.Name == projectDto.Name || x.Uid == projectDto.Uid)))
            {
                return new ProjectCreateUpdateResultDto
                {
                    CreateUpdateResult = ServiceActionResult.Failed,
                    Message = "Tên dự án đã tồn tại."
                };
            }

            if (projectDto.Id <= 0)
            {
                var projectEntity = _mapper.Map<Project>(projectDto);

                _projectRepo.Add(projectEntity);
                var effectedRecord = _realEstateDbContext.SaveChanges();

                if (effectedRecord == 1)
                {
                    return new ProjectCreateUpdateResultDto
                    {
                        Id = projectEntity.Id,
                        CreateUpdateResult = ServiceActionResult.Successed,
                        Message = "Lưu thành công."
                    };
                }
            }
            else
            {
                var projectEntity = await _projectRepo.GetByIdAsync(projectDto.Id);

                if (projectEntity == null)
                {
                    return new ProjectCreateUpdateResultDto
                    {
                        CreateUpdateResult = ServiceActionResult.NotFound,
                        Message = "Không tìm thấy dữ liệu."
                    };
                }

                projectEntity = _mapper.Map(projectDto, projectEntity);

                _projectRepo.Update(projectEntity);
                var effectedRecord = _realEstateDbContext.SaveChanges();

                if (effectedRecord == 1)
                {
                    return new ProjectCreateUpdateResultDto
                    {
                        Id = projectEntity.Id,
                        CreateUpdateResult = ServiceActionResult.Successed,
                        Message = "Lưu thành công."
                    };
                }
            }

            return new ProjectCreateUpdateResultDto
            {
                CreateUpdateResult = ServiceActionResult.Failed,
                Message = "Lưu thất bại."
            };
        }

        public async Task<TableData<ProjectTableDataDto>> GetProjectTableData(TableParameterDto tableParameter)
        {
            var data =
                await _projectRepo.GetTableDataAsync(tableParameter,
                    sortByDict: new Dictionary<string, Expression<Func<Project, object>>>
                    {
                        { "id", (x) => x.Id },
                        { "uid", (x) => x.Uid },
                        { "name", (x) => x.Name },
                        { "invesloperName", (x) => x.Invesloper.Name },
                        { "type", (x) => x.Type },
                        { "block", (x) => x.Block },
                        { "unit", (x) => x.Unit }
                    },
                    includes: new Expression<Func<Project, object>>[] {
                        (x) => x.Invesloper
                    }
                );
            return _mapper.Map<TableData<ProjectTableDataDto>>(data);
        }

        public async Task<ProjectDeleteResultDto> DeleteProjectAsync(int id)
        {
            var project = await _projectRepo.GetByIdAsync(id);

            if (project == null)
            {
                return new ProjectDeleteResultDto
                {
                    DeleteResult = ServiceActionResult.NotFound,
                    Message = "Không tìm thấy thông tin."
                };
            }

            _projectRepo.Delete(project);

            var result = await _realEstateDbContext.SaveChangesAsync();

            if (result == 1)
            {
                return new ProjectDeleteResultDto
                {
                    DeleteResult = ServiceActionResult.Successed,
                    Message = "Xóa thành công."
                };
            }

            return new ProjectDeleteResultDto
            {
                DeleteResult = ServiceActionResult.Failed,
                Message = "Xóa thất bại."
            };
        }

        public async Task<List<HomePageProjectDto>> GetHomePageProjects()
        {
            var project = await _projectRepo.GetAllAsync((include) =>
            {
                include.Add(x => x.DisplayImage);
            }).ToList();

            return project != null ? _mapper.Map<List<HomePageProjectDto>>(project) : null;
        }

        public async Task<ProjectClientDto> GetProjectAsync(string uid)
        {
            var project = await _projectRepo.GetAllAsync((include) =>
            {
                include.Add(x => x.Images);
                include.Add(x => x.Invesloper);

            }).FirstOrDefault(x => x.Uid == uid);

            return _mapper.Map<ProjectClientDto>(project);
        }
    }
}
