﻿using AutoMapper;
using RealEstate.Core.Contracts.Services;
using RealEstate.Core.Dtos.Commons;
using RealEstate.Core.Dtos.Image;
using RealEstate.Core.Dtos.Table;
using RealEstate.Data.DBContext;
using RealEstate.Data.Entities;
using RealEstate.Data.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace RealEstate.Services.Services
{
    public class ImageService : IImageService
    {
        private readonly ITableRepository<Image> _imageRepo;
        private readonly IMapper _mapper;
        private readonly RealEstateDbContext _dbContext;
        public ImageService(ITableRepository<Image> imageRepository, RealEstateDbContext dbContext, IMapper mapper)
        {
            _imageRepo = imageRepository;
            _mapper = mapper;
            _dbContext = dbContext;
        }

        public Task<ServiceActionResult> CreateImage(List<ImageDto> imagesParameter)
        {
            var entities = _mapper.Map<List<Image>>(imagesParameter);
            _imageRepo.AddRange(entities);

            var effectedCount = _dbContext.SaveChanges();

            if (effectedCount > 0)
            {
                return Task.FromResult(ServiceActionResult.Successed);
            }

            return Task.FromResult(ServiceActionResult.Failed);
        }

        public async Task<ServiceActionResult> DeleteImageAsync(int id)
        {
            var image = await _imageRepo.GetByIdAsync(id);

            if (image == null)
            {
                return ServiceActionResult.NotFound;
            }
            _imageRepo.Delete(image);

            var effectedRow = await _dbContext.SaveChangesAsync();

            return (effectedRow == 0) ? ServiceActionResult.Failed : ServiceActionResult.Successed;
        }

        public async Task<int> GetImageCount()
        {
            var image = await _imageRepo.GetAllAsync().LastOrDefault();
            if (image != null)
            {
                return image.Id;
            }

            return 0;
        }

        public async Task<TableData<ImageTableDataDto>> GetImageTableDataAsync(TableParameterDto tableParameter)
        {
            var data = await _imageRepo.GetTableDataAsync(tableParameter,
                    sortByDict: new Dictionary<string, Expression<Func<Image, object>>>
                    {
                        { "id", (x) => x.Id },
                        { "name", (x) => x.Name },
                        { "projectName", (x) => x.Project.Name }
                    },
                    includes: new Expression<Func<Image, object>>[1] {
                        (x) => x.Project
                    });

            return _mapper.Map<TableData<ImageTableDataDto>>(data);
        }
    }
}
