﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using RealEstate.Core.Contracts.Services;
using RealEstate.Core.Dtos.Commons;
using RealEstate.Core.Dtos.Invesloper;
using RealEstate.Core.Dtos.Table;
using RealEstate.Data.DBContext;
using RealEstate.Data.Entities;
using RealEstate.Data.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace RealEstate.Services.Services
{
	public class InvesloperService : IInvesloperService
	{
		private readonly RealEstateDbContext _realEstateDbContext;
		private readonly ITableRepository<Invesloper> _invesloperRepository;
		private readonly IMapper _mapper;

		public InvesloperService(
			RealEstateDbContext realEstateDbContext,
			ITableRepository<Invesloper> invesloperRepository,
			IMapper mapper
		)
		{
			_realEstateDbContext = realEstateDbContext;
			_invesloperRepository = invesloperRepository;
			_mapper = mapper;
		}

		public async Task<TableData<InvesloperTableDataDto>> GetInveslopersAsync(TableParameterDto tableParams)
		{
			var inveslopers = await _invesloperRepository.GetTableDataAsync(tableParams, sortByDict: new Dictionary<string, Expression<Func<Invesloper, object>>> {
				{ "id", (x) => x.Id },
				{ "name", (x) => x.Name },
				{ "url", (x) => x.Url }
			});

			return _mapper.Map<TableData<InvesloperTableDataDto>>(inveslopers);
		}

		public async Task<InvesloperDetailsDto> GetInvesloperDetailsAsync(int id)
		{
			var invesDetails = await _invesloperRepository.GetByIdAsync(id);

			return _mapper.Map<InvesloperDetailsDto>(invesDetails);
		}

		public async Task<int> CreateOrUpdateInvesloperAsync(InvesloperDetailsDto invesloperDetails)
		{
			var invlopEntity = _mapper.Map<Invesloper>(invesloperDetails);

			if (invlopEntity.Id > 0)
			{
				_invesloperRepository.Update(invlopEntity);
			}
			else
			{
				_invesloperRepository.Add(invlopEntity);
			}

			await _realEstateDbContext.SaveChangesAsync();

			return invlopEntity.Id;
		}

		public async Task<InvesloperDeleteResultDto> DeleteInvesloperAsync(int id)
		{
			// move delete workflow to repo instead ??

			var invlop = await _invesloperRepository.DbSet.Include(x => x.Projects).FirstOrDefaultAsync(x => x.Id == id);

			if (invlop == null)
			{
				return new InvesloperDeleteResultDto
				{
					DeleteResult = ServiceActionResult.NotFound,
					Message = "Không tìm thấy thông tin."
				};
			}

			if (invlop.Projects.Count() > 0)
			{
				return new InvesloperDeleteResultDto
				{
					DeleteResult = ServiceActionResult.Failed,
					Message = "Không thể xóa - Một số dự án đang sử dụng thông tin nhà đầu tư này."
				};
			}

			_invesloperRepository.Delete(invlop);

			var result = await _realEstateDbContext.SaveChangesAsync();

			if (result == 1)
			{
				return new InvesloperDeleteResultDto
				{
					DeleteResult = ServiceActionResult.Successed,
					Message = "Xóa thành công."
				};
			}

			return new InvesloperDeleteResultDto
			{
				DeleteResult = ServiceActionResult.Failed,
				Message = "Xóa thất bại."
			};
		}
	}
}
