﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RealEstate.Core.Identity;
using RealEstate.Data.DBContext;
using RealEstate.Data.Repositories;
using RealEstate.Data.Repositories.Contracts;

namespace RealEstate.Data.Extensions
{
	public static class DataModuleServiceCollectionExtension
	{
		public static IServiceCollection AddDataModule(this IServiceCollection services)
		{
			var serviceProvider = services.BuildServiceProvider();
			var configuration = serviceProvider.GetRequiredService<IConfiguration>();

			// Configure DbContext
			services.AddDbContext<RealEstateDbContext>(options =>
			{
				options.UseSqlServer(GetConnectionString());
			});

			services.AddIdentity<User, Role>().AddEntityFrameworkStores<RealEstateDbContext>();

			// Repository
			services.AddScoped(typeof(IRepository<>), typeof(BaseRepository<>));
			services.AddScoped(typeof(IRepository<,>), typeof(BaseRepository<,,>));

			services.AddScoped(typeof(ITableRepository<>), typeof(TableRepository<>));
			services.AddScoped(typeof(ITableRepository<,>), typeof(TableRepository<,,>));

			return services;

			string GetConnectionString() => configuration.GetConnectionString("DefaultConnection");
		}
	}
}
