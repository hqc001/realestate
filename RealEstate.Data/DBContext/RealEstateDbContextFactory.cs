﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using RealEstate.Data.DBContext;
using System;
using System.IO;

namespace CodeForFun.Data.DbContexts
{
	public class RealEstateDbContextFactory : IDesignTimeDbContextFactory<RealEstateDbContext>
	{
		public RealEstateDbContext CreateDbContext(string[] args)
		{
			var projectPath = Directory.GetCurrentDirectory().Split("bin\\", StringSplitOptions.None)[0];
			var configuration = new ConfigurationBuilder()
				.SetBasePath(projectPath)
				.AddJsonFile("appsettings.Development.json")
				.Build();

			var connectionString = configuration.GetConnectionString("DefaultConnection");

			var optionsBuilder = new DbContextOptionsBuilder<RealEstateDbContext>();
			optionsBuilder.UseSqlServer(connectionString);

			return new RealEstateDbContext(optionsBuilder.Options);
		}
	}
}
