﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using RealEstate.Core.Identity;
using RealEstate.Data.Extensions;

namespace RealEstate.Data.DBContext
{
	public class RealEstateDbContext : IdentityDbContext<User, Role, int>
	{
		public RealEstateDbContext(DbContextOptions<RealEstateDbContext> options) : base(options)
		{
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
			modelBuilder.AddEntityConfigurationsFromAssembly();
		}
	}
}
