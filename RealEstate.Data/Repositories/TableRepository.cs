﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using RealEstate.Core.Dtos.Table;
using RealEstate.Data.DBContext;
using RealEstate.Data.Entities;
using RealEstate.Data.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace RealEstate.Data.Repositories
{
	public class TableRepository<TContext, TEntity, TId> : BaseRepository<TContext, TEntity, TId>, ITableRepository<TEntity, TId>
		where TEntity : BaseEntity<TId>
		where TContext : DbContext
		where TId : struct
	{
		public TableRepository(TContext context, IHttpContextAccessor httpContextAccessor) : base(context, httpContextAccessor)
		{
		}

		public async Task<TableData<TEntity>> GetTableDataAsync(TableParameterDto tableParameter, Dictionary<string, Expression<Func<TEntity, object>>> sortByDict)
		{
			var query = DbSet.AsNoTracking();
			return await GetTableDataAsync(query, tableParameter, sortByDict);
		}

		public async Task<TableData<TEntity>> GetTableDataAsync(TableParameterDto tableParameter, Dictionary<string, Expression<Func<TEntity, object>>> sortByDict, Expression<Func<TEntity, bool>> predicate)
		{
			var query = DbSet.AsNoTracking().Where(predicate);
			return await GetTableDataAsync(query, tableParameter, sortByDict);
		}

		public async Task<TableData<TEntity>> GetTableDataAsync(TableParameterDto tableParameter, Dictionary<string, Expression<Func<TEntity, object>>> sortByDict, Expression<Func<TEntity, object>>[] navigationPropertyPaths)
		{
			var query = QueryWithInclude(DbSet.AsNoTracking(), navigationPropertyPaths);
			return await GetTableDataAsync(query, tableParameter, sortByDict);
		}

		public async Task<TableData<TEntity>> GetTableDataAsync(TableParameterDto tableParameter, Dictionary<string, Expression<Func<TEntity, object>>> sortByDict, Expression<Func<TEntity, object>>[] navigationPropertyPaths, Expression<Func<TEntity, bool>> predicate)
		{
			var query = QueryWithInclude(DbSet.AsNoTracking().Where(predicate), navigationPropertyPaths);
			return await GetTableDataAsync(query, tableParameter, sortByDict);
		}

		private async Task<TableData<TEntity>> GetTableDataAsync(IQueryable<TEntity> query, TableParameterDto tableParameter, Dictionary<string, Expression<Func<TEntity, object>>> sortByDict)
		{
			query = PagingSorting(query, tableParameter, sortByDict);
			return await GetTableDataAsync(query);
		}

		private async Task<TableData<TEntity>> GetTableDataAsync(IQueryable<TEntity> query)
		{
			return new TableData<TEntity>
			{
				Data = await query.ToArrayAsync(),
				TotalRecord = await DbSet.CountAsync()
			};
		}

		private IQueryable<TEntity> QueryWithInclude(IQueryable<TEntity> query, Expression<Func<TEntity, object>>[] navigationPropertyPaths)
		{
			foreach (var included in navigationPropertyPaths)
			{
				query = query.Include(included);
			}
			return query;
		}

		private IQueryable<TEntity> PagingSorting(IQueryable<TEntity> query, TableParameterDto tableParameter, Dictionary<string, Expression<Func<TEntity, object>>> sortByDict)
		{
			sortByDict.TryGetValue(tableParameter.SortBy ?? "", out var sortByFunc);

			if (sortByFunc != null && !string.IsNullOrWhiteSpace(tableParameter.SortBy) && !string.IsNullOrWhiteSpace(tableParameter.SortDirection))
			{
				query = tableParameter.SortDirection == "asc" ? query.OrderBy(sortByFunc) : query.OrderByDescending(sortByFunc);
			}

			return query.Skip(tableParameter.PageSize * (tableParameter.PageIndex)).Take(tableParameter.PageSize);
		}
	}

	public class TableRepository<TEntity> : TableRepository<RealEstateDbContext, TEntity, int>, ITableRepository<TEntity> where TEntity : BaseEntity<int>
	{
		public TableRepository(RealEstateDbContext context, IHttpContextAccessor httpContextAccessor) : base(context, httpContextAccessor)
		{
		}
	}
}
