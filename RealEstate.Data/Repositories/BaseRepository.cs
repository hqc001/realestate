﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using RealEstate.Data.DBContext;
using RealEstate.Data.Entities;
using RealEstate.Data.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Threading.Tasks;

namespace RealEstate.Data.Repositories
{
	public class BaseRepository<TContext, TEntity, TId> : IRepository<TEntity, TId>
		where TEntity : BaseEntity<TId>
		where TContext : DbContext
		where TId : struct
	{
		protected readonly TContext _context;
		protected readonly IHttpContextAccessor _httpContextAccessor;

		public BaseRepository(TContext context, IHttpContextAccessor httpContextAccessor)
		{
			_context = context;
			_httpContextAccessor = httpContextAccessor;
		}

		public DbSet<TEntity> DbSet => _context.Set<TEntity>();

		public IAsyncEnumerable<TEntity> GetAllAsync() => DbSet.ToAsyncEnumerable();

        public IAsyncEnumerable<TEntity> GetAllAsync(Action<List<Expression<Func<TEntity, object>>>> includeOptions)
        {
            var query = ExecuteInclude(DbSet.AsNoTracking().AsQueryable(), includeOptions);
            return query.ToAsyncEnumerable();
        }

        public Task<TEntity> GetByIdAsync(TId id) => DbSet.SingleOrDefaultAsync(x => x.Id.Equals(id));

        public Task<TEntity> GetByIdAsync(TId id, Action<List<Expression<Func<TEntity, object>>>> includeOptions)
        {
            var query = ExecuteInclude(DbSet.Where(x => x.Id.Equals(id)), includeOptions);
            return query.SingleOrDefaultAsync();
        }

        public EntityEntry Add(TEntity entity)
		{
			SetAuditInfo(entity);
			return DbSet.Add(entity);
		}

        public void AddRange(IEnumerable<TEntity> entities)
        {
            foreach (var item in entities)
            {
                SetAuditInfo(item, isUpdate: true);
            }

            DbSet.AddRange(entities);
        }

        public EntityEntry Update(TEntity entity)
		{
			SetAuditInfo(entity, isUpdate: true);
			return DbSet.Update(entity);
		}

		public EntityEntry Delete(TEntity entity) => DbSet.Remove(entity);

		public bool IsExist(Expression<Func<TEntity, bool>> expression) => DbSet.Any(expression);

		private void SetAuditInfo(TEntity entity, bool isUpdate = false)
		{
			if (typeof(AuditEntity<TId>).IsAssignableFrom(typeof(TEntity)))
			{
				var auditEntity = entity as AuditEntity<TId>;
                // httpContextAccessor shouldn't be here, may be find other way to get current user id
                var currentUserId = _httpContextAccessor.HttpContext?.User?.FindFirst(ClaimTypes.NameIdentifier)?.Value;

				if (currentUserId == null)
				{
					throw new Exception("User not found.");
				}

                var canParse = int.TryParse(currentUserId, out var userId);
                if (canParse)
                {
                    UpdateAuditData(auditEntity, userId);
                }
            }

			void UpdateAuditData(AuditEntity<TId> auditEntity, int value)
			{
				if (isUpdate)
				{
					auditEntity.UpdatedBy = value;
					auditEntity.UpdatedDate = DateTime.Now;
				}
				else
				{
					auditEntity.CreatedBy = value;
					auditEntity.CreateDate = DateTime.Now;
				}
			}
		}

        private IQueryable<TEntity> ExecuteInclude(IQueryable<TEntity> query, Action<List<Expression<Func<TEntity, object>>>> includeOptions)
        {
            var includes = new List<Expression<Func<TEntity, object>>>();
            includeOptions.Invoke(includes);
            includes.ForEach(incl => { query = query.Include(incl); });

            return query;
        }
    }

	public class BaseRepository<TEntity> : BaseRepository<RealEstateDbContext, TEntity, int>, IRepository<TEntity> where TEntity : BaseEntity<int>
	{
		public BaseRepository(RealEstateDbContext context, IHttpContextAccessor httpContextAccessor) : base(context, httpContextAccessor)
		{
		}
	}
}
