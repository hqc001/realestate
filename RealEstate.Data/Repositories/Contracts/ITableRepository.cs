﻿using RealEstate.Core.Dtos.Table;
using RealEstate.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace RealEstate.Data.Repositories.Contracts
{
	public interface ITableRepository<TEntity, TId> : IRepository<TEntity, TId> where TEntity : BaseEntity<TId>
	{
		Task<TableData<TEntity>> GetTableDataAsync(TableParameterDto tableParameter, Dictionary<string, Expression<Func<TEntity, object>>> sortByDict);
		Task<TableData<TEntity>> GetTableDataAsync(TableParameterDto tableParameter, Dictionary<string, Expression<Func<TEntity, object>>> sortByDict, Expression<Func<TEntity, bool>> predicate);
		Task<TableData<TEntity>> GetTableDataAsync(TableParameterDto tableParameter, Dictionary<string, Expression<Func<TEntity, object>>> sortByDict, Expression<Func<TEntity, object>>[] includes);
		Task<TableData<TEntity>> GetTableDataAsync(TableParameterDto tableParameter, Dictionary<string, Expression<Func<TEntity, object>>> sortByDict, Expression<Func<TEntity, object>>[] includes, Expression<Func<TEntity, bool>> predicate);
	}

	public interface ITableRepository<TEntity> : ITableRepository<TEntity, int> where TEntity : BaseEntity<int>
	{
	}
}
