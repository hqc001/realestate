﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using RealEstate.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace RealEstate.Data.Repositories.Contracts
{
    public interface IRepository<TEntity, TId> where TEntity : BaseEntity<TId>
    {
        DbSet<TEntity> DbSet { get; }

        Task<TEntity> GetByIdAsync(TId id);

        Task<TEntity> GetByIdAsync(TId id, Action<List<Expression<Func<TEntity, object>>>> includes);

        IAsyncEnumerable<TEntity> GetAllAsync();

        IAsyncEnumerable<TEntity> GetAllAsync(Action<List<Expression<Func<TEntity, object>>>> includes);

        EntityEntry Add(TEntity entity);

        void AddRange(IEnumerable<TEntity> entity);

        EntityEntry Update(TEntity entity);

        EntityEntry Delete(TEntity entity);

        bool IsExist(Expression<Func<TEntity, bool>> expression);
    }

    public interface IRepository<TEntity> : IRepository<TEntity, int> where TEntity : BaseEntity<int>
    {
    }
}
