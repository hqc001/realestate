﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RealEstate.Data.Entities;

namespace RealEstate.Data.Mapping
{
	public class BlogEntityMapping : IEntityTypeConfiguration<Blog>
	{
		public void Configure(EntityTypeBuilder<Blog> builder)
		{
			builder.ToTable("Blog")
				.HasKey(b => b.Id);
			builder.HasOne(x => x.Project).WithMany(y => y.Blogs).HasForeignKey(z => z.ProjectId);
		}
	}
}
