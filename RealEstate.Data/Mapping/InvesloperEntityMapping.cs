﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RealEstate.Data.Entities;

namespace RealEstate.Data.Mapping
{
	public class InvesloperEntityMapping : IEntityTypeConfiguration<Invesloper>
	{
		public void Configure(EntityTypeBuilder<Invesloper> builder)
		{
			builder.ToTable("Invesloper")
				.HasKey(x => x.Id);

		}
	}
}
