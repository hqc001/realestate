﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RealEstate.Data.Entities;

namespace RealEstate.Data.Mapping
{
    public class ProjectEntityMapping : IEntityTypeConfiguration<Project>
    {
        public void Configure(EntityTypeBuilder<Project> builder)
        {
            builder.ToTable("Project")
                .HasKey(p => p.Id);

            builder.HasMany(x => x.Images).WithOne(y => y.Project);
            builder.HasMany(x => x.Blogs).WithOne(y => y.Project);

            builder.HasOne(x => x.Invesloper).WithMany(y => y.Projects).HasForeignKey(z => z.InvesloperId);
            builder.HasOne(x => x.DisplayImage)
                   .WithMany()
                   .HasForeignKey(x => x.DisplayImageId)
                   .OnDelete(DeleteBehavior.ClientSetNull);
        }
    }
}
