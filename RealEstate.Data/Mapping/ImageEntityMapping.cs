﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RealEstate.Data.Entities;

namespace RealEstate.Data.Mapping
{
	public class ImageEntityMapping : IEntityTypeConfiguration<Image>
	{
		public void Configure(EntityTypeBuilder<Image> builder)
		{
			builder.ToTable("Image")
				.HasKey(x => x.Id);

			builder.HasOne(x => x.Project).WithMany(y => y.Images);
        }
	}
}
