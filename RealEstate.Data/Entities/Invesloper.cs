﻿using System.Collections.Generic;

namespace RealEstate.Data.Entities
{
    public class Invesloper : BaseEntity<int>
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public virtual IEnumerable<Project> Projects { get; set; }
    }
}
