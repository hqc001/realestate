﻿using RealEstate.Core.Constants.Enums;
using System;
using System.Collections.Generic;

namespace RealEstate.Data.Entities
{
    public class Project : AuditEntity<int>
    {
        public string Uid { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string District { get; set; }
        public string City { get; set; }
        public ProjectStatuses StatusId { get; set; }
        public int InvesloperId { get; set; }
        public ProjectTypes Type { get; set; }
        public int Block { get; set; }
        public string Floor { get; set; }
        public int Unit { get; set; }
        public string Price { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime HandoverDate { get; set; }
        public string Penthouse { get; set; }
        public float DensityOfBuilding { get; set; }
        public string Area { get; set; }
        public float Lat { get; set; }
        public float Lng { get; set; }
        public string Address { get; set; }
        /// <summary> Tiện ích ngoại khu </summary>
        public string InsideUtilities { get; set; }
		/// <summary> Tiện ích ngoại khu </summary>
		public string OutsideUtilities { get; set; }
		/// <summary> Thiết kế </summary>
		public string Design { get; set; }
		/// <summary> Phụ lục </summary>
		public string Appendix { get; set; }
		/// <summary> Ưu đãi </summary>
		public string Preferential { get; set; }
        /// <summary> Thông tin địa điểm </summary>
		public string LocationDescription { get; set; }
        /// <summary> Giới thiệu </summary>
		public string Introduction { get; set; }
        /// <summary> Image that display in main page </summary>
        public int? DisplayImageId { get; set; }
        public bool IsGoodPrice { get; set; }

        public Image DisplayImage { get; set; }
        public Invesloper Invesloper { get; set; }
        public IEnumerable<Image> Images { get; set; }
        public IEnumerable<Blog> Blogs { get; set; }
    }
}
