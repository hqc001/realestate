﻿namespace RealEstate.Data.Entities
{
	public class Blog : AuditEntity<int>
	{
		public string UId { get; set; }
		public int ProjectId { get; set; }
		public string Title { get; set; }
		public string Detail { get; set; }
		public string Summary { get; set; }
		public Project Project { get; set; }
	}
}
