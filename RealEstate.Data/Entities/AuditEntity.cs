﻿using System;

namespace RealEstate.Data.Entities
{
	public class AuditEntity<TEntityId> : BaseEntity<TEntityId>
	{
		public int CreatedBy { get; set; }
		public DateTime CreateDate { get; set; }
		public int? UpdatedBy { get; set; }
		public DateTime? UpdatedDate { get; set; }
	}
}
