﻿namespace RealEstate.Data.Entities
{
    public class Image : BaseEntity<int>
    {
        public int ProjectId { get; set; }
        public string Name { get; set; }
        public string ThumbnailName { get; set; }
        public string Description { get; set; }
        public virtual Project Project { get; set; }
    }
}
