﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.CommandLineUtils;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MigrationTool.Services;
using RealEstate.Core.Identity;
using RealEstate.Data.DBContext;
using RealEstate.Data.Extensions;
using System;
using System.IO;

namespace MigrationTool
{
	class Program
	{
		static void Main(string[] args)
		{
			var commandLineApp = new CommandLineApplication
			{
				Name = "dotnet MigrationTool.dll",
				Description = "Internal dev tool for migration"
			};

			commandLineApp.HelpOption("-h|--help");

			var serviceProvider = SetupDI();

			commandLineApp.Command("add-default-account", (config) =>
			{
				config.Description = "Add default admin account";
				config.OnExecute(() =>
				{
					var accountDataSeeding = serviceProvider.GetRequiredService<AccountDataSeedingService>();
					return accountDataSeeding.Execute();
				});
			});

			commandLineApp.OnExecute(() =>
			{
				commandLineApp.ShowHelp();
				return 0;
			});

			commandLineApp.Execute(args);
			
			if(args.Length > 0) {
				Console.WriteLine("\nPress any key to continue ...");
				Console.ReadLine();
			}
		}

		private static ServiceProvider SetupDI() {
			var services = new ServiceCollection();
			var currentDirectory = Directory.GetCurrentDirectory().Split("bin\\", StringSplitOptions.None)[0];
			var parentPath = Directory.GetParent(currentDirectory).Parent.FullName;
			var projectPath = Path.Combine(parentPath, "RealEstate");

			var configuration = new ConfigurationBuilder()
				.SetBasePath(projectPath)
				.AddJsonFile("appsettings.Development.json")
				.Build();

			services.AddSingleton<IConfiguration>(configuration);
			
			services.AddDataModule();

			services.AddTransient<AccountDataSeedingService>();

			return services.BuildServiceProvider();
		}
	}
}
