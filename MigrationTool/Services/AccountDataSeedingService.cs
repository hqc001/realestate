﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using RealEstate.Core.Identity;
using RealEstate.Data.DBContext;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MigrationTool.Services
{
	public class AccountDataSeedingService
	{
		private readonly RealEstateDbContext _realEstateDbContext;
		private readonly UserManager<User> _userManager;
		private readonly RoleManager<Role> _roleManager;

		public AccountDataSeedingService(
			RealEstateDbContext realEstateDbContext,
			UserManager<User> userManager,
			RoleManager<Role> roleManager
		)
		{
			_realEstateDbContext = realEstateDbContext;
			_userManager = userManager;
			_roleManager = roleManager;
		}

		public async Task<int> Execute()
		{
			using (var transaction = _realEstateDbContext.Database.BeginTransaction())
			{
				Console.WriteLine("Execute seeding account data..");

				var defaultRoles = new string[] {
					"Admin", "User"
				};

				var allRoles = _roleManager.Roles.AsNoTracking().Select(x => x.Name).ToList();
				var defaultRole = defaultRoles.Where(x => !allRoles.Contains(x));

				foreach (var role in defaultRole)
				{
					await _roleManager.CreateAsync(new Role { Name = role });
				}

				var adminUser = await _userManager.FindByNameAsync("admin");
				if (adminUser == null)
				{
					var user = new User
					{
						UserName = "admin",
						Email = "admin@admin.com",
						LockoutEnabled = false
					};

					await _userManager.CreateAsync(user, "Abc12345!");
					await _userManager.AddToRoleAsync(user, "Admin");
				}

				transaction.Commit();
			}

			return 1;
		}
	}
}
