export const environment = {
	production: true,
	baseUrl: 'https://www.canhosg24h.net/admin',
	tinyMceSkinPath: '/admin/assets/tinymce/skins/lightgray',
	tinyMceBasePath: '/assets/tinymce',
	uploadUrl: 'https://www.canhosg24h.net/upload/'
};
