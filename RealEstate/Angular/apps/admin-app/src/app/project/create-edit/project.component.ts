import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { IProject, IProjectDetailsForm } from '@codeforfun/admin/models';
import { ProjectDetailsFeatureService, SnackBarMessageService } from '@codeforfun/admin/services';
import { Utilities } from '@codeforfun/admin/utilities';
import { merge, of, Subscription } from 'rxjs';
import { distinctUntilChanged, switchMap } from 'rxjs/operators';

@Component({
	selector: 'admin-app-project',
	templateUrl: './project.component.html',
	styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnDestroy, OnInit {
	public selectedIndex = 0;
	public projectForm: FormGroup;
	public title = '';

	private _titles = {
		add: 'Thêm mới dự án',
		edit: 'Chỉnh sửa thông tin dự án'
	};

	private _subscriptions: Subscription[] = [];
	private _tabIndex: IProjectDetailsForm = {
		projectDetails: 0,
		projectIntroduction: 1,
		projectInsideUtilities: 2,
		projectOutsideUtilities: 3,
		projectDesign: 4,
		projectAppendix: 5,
		projectPreferential: 6,
		projectLocation: 7
	};
	private _isSubmitted = false;

	constructor(
		private _formBuilder: FormBuilder,
		private _snackBarService: SnackBarMessageService,
		private _activatedRoute: ActivatedRoute,
		public projDtFtSv: ProjectDetailsFeatureService
	) {
		this._subscriptions.push(
			this.projDtFtSv.formData$.subscribe(data => {
				this.title = this._isEditMode ? this._titles.edit : this._titles.add;

				if (!this.projectForm) {
					this.projectForm = Utilities.buildForm<IProjectDetailsForm>(this._formBuilder, {
						projectDetails: Utilities.buildForm<IProject>(this._formBuilder, {
							id: [data.project.id],
							uid: [data.project.uid],
							code: [data.project.code],
							area: [data.project.area, Validators.required],
							block: [data.project.block, Validators.required],
							city: [data.project.city, Validators.required],
							densityOfBuilding: [data.project.densityOfBuilding, Validators.required],
							district: [data.project.district, Validators.required],
							floor: [data.project.floor, Validators.required],
							handoverDate: [data.project.handoverDate, Validators.required],
							invesloperId: [data.project.invesloperId, Validators.required],
							name: [data.project.name, Validators.required],
							penthouse: [data.project.penthouse, Validators.required],
							price: [data.project.price, Validators.required],
							startDate: [data.project.startDate, Validators.required],
							statusId: [data.project.statusId, Validators.required],
							type: [data.project.type, Validators.required],
							unit: [data.project.unit, Validators.required],
							displayImageId: [data.project.displayImageId],
							isGoodPrice: [data.project.isGoodPrice],
							address: [data.project.address]
						}),
						projectIntroduction: Utilities.buildForm<IProject>(this._formBuilder, {
							introduction: [data.project.introduction, Validators.required]
						}),
						projectAppendix: Utilities.buildForm<IProject>(this._formBuilder, {
							appendix: [data.project.appendix, Validators.required]
						}),
						projectDesign: Utilities.buildForm<IProject>(this._formBuilder, {
							design: [data.project.design, Validators.required]
						}),
						projectInsideUtilities: Utilities.buildForm<IProject>(this._formBuilder, {
							insideUtilities: [data.project.insideUtilities, Validators.required]
						}),
						projectOutsideUtilities: Utilities.buildForm<IProject>(this._formBuilder, {
							outsideUtilities: [data.project.outsideUtilities, Validators.required]
						}),
						projectPreferential: Utilities.buildForm<IProject>(this._formBuilder, {
							preferential: [data.project.preferential, Validators.required]
						}),
						projectLocation: Utilities.buildForm<IProject>(this._formBuilder, {
							lat: [data.project.lat, Validators.required],
							lng: [data.project.lng, Validators.required],
							locationDescription: [data.project.locationDescription]
						})
					});
				} else {
					this.projectForm.reset({
						projectDetails: data.project,
						projectIntroduction: data.project,
						projectAppendix: data.project,
						projectDesign: data.project,
						projectInsideUtilities: data.project,
						projectOutsideUtilities: data.project,
						projectPreferential: data.project,
						projectLocation: data.project
					});
				}
			})
		);
	}

	public ngOnDestroy() {
		this.projDtFtSv.resetTabErrorState();
		this.projDtFtSv.resetFormData();
		Utilities.unsubcribe(this._subscriptions);
	}

	public ngOnInit() {
		this._subscriptions.push(
			merge(
				this._getFormStatusChangeObs('projectAppendix'),
				this._getFormStatusChangeObs('projectIntroduction'),
				this._getFormStatusChangeObs('projectDesign'),
				this._getFormStatusChangeObs('projectDetails'),
				this._getFormStatusChangeObs('projectInsideUtilities'),
				this._getFormStatusChangeObs('projectOutsideUtilities'),
				this._getFormStatusChangeObs('projectPreferential'),
				this._getFormStatusChangeObs('projectLocation')
			).subscribe(e => {
				this.projDtFtSv.setTabErrorState(e.name, e.status === 'INVALID' && this._isSubmitted);
			})
		);
	}

	public saveProjectDetails() {
		this._isSubmitted = true;

		Utilities.updateValueAndValidity(this.projectForm);

		this._setTabErrorState();

		if (this.projectForm.invalid) {
			return this._snackBarService.showMessage('Hãy điền đầy đủ thông tin bắt buộc.', 'error');
		}

		// collect data from all small forms
		this.projDtFtSv.createOrUpdateProject({
			...this._getForm('projectDetails').getRawValue(),
			...this._getForm('projectAppendix').getRawValue(),
			...this._getForm('projectIntroduction').getRawValue(),
			...this._getForm('projectDesign').getRawValue(),
			...this._getForm('projectInsideUtilities').getRawValue(),
			...this._getForm('projectOutsideUtilities').getRawValue(),
			...this._getForm('projectPreferential').getRawValue(),
			...this._getForm('projectLocation').getRawValue()
		});
	}

	private _getForm(name: keyof IProjectDetailsForm) {
		return this.projectForm.get(name) as FormGroup;
	}

	private _getFormStatusChangeObs(name: keyof IProjectDetailsForm) {
		return this._getForm(name).statusChanges.pipe(
			distinctUntilChanged(),
			switchMap(status => of({ name, status }))
		);
	}

	private _setTabErrorState() {
		Object.keys(this.projectForm.controls).forEach(key => {
			this.projDtFtSv.setTabErrorState(key, this.projectForm.get(key).status === 'INVALID');
		});

		const idx = this._tabIndex[Object.keys(this._tabIndex).find(x => this.projDtFtSv.tabErrorState[x])];
		this.selectedIndex = idx != null ? idx : this.selectedIndex;
	}

	private get _isEditMode() {
		return +this._activatedRoute.snapshot.params['id'] ? true : false;
	}
}
