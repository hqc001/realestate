import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AppFeatureService, ProjectDetailsFeatureService } from '@codeforfun/admin/services';
import { Utilities } from '@codeforfun/admin/utilities';
import { Subscription } from 'rxjs';
import { distinctUntilChanged, take } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';

@Component({
	selector: 'admin-app-project-details',
	templateUrl: './project-details.component.html',
	styles: [
		`
			::ng-deep .select-image {
				height: auto !important;
			}
			::ng-deep .select-image .mat-option-text {
				display: flex;
				justify-content: center;
			}
		`
	]
})
export class ProjectDetailsComponent implements OnDestroy, OnInit {
	@Input() public formGroup: FormGroup;

	private subs: Subscription[] = [];

	private get cityControl() {
		return this.formGroup.get('city');
	}

	private get displayImageControl() {
		return this.formGroup.get('displayImageId');
	}

	private get districtControl() {
		return this.formGroup.get('district');
	}

	constructor(public projDtFtSv: ProjectDetailsFeatureService, public appFtSv: AppFeatureService) {}

	public ngOnDestroy(): void {
		this.appFtSv.resetDistricts();
		Utilities.unsubcribe(this.subs);
	}

	public ngOnInit() {
		const _city = this.cityControl.value;
		if (_city) {
			this.appFtSv.getDistrictsByCityKey(_city);
		}

		this.subs.push(
			this.cityControl.valueChanges.pipe(distinctUntilChanged()).subscribe(city => {
				if (city) {
					this.districtControl.reset(null, { emitEvent: false });
					this.appFtSv.getDistrictsByCityKey(city);
				}
			})
		);
	}

	public getThumbnaiImage(thumbnailName: string) {
		return environment.uploadUrl + thumbnailName;
	}

	public getSelectedThumbnaiImage() {
		let rs = '';

		this.projDtFtSv.formData$.pipe(take(1)).subscribe(data => {
			if (data != null && data.projectImages != null) {
				const image = data.projectImages.find(x => x.value === this.displayImageControl.value);
				if (image != null) {
					rs = environment.uploadUrl + image.description;
				}
			}
		});

		return rs;
	}
}
