import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
	selector: 'admin-app-project-design',
	templateUrl: './project-design.component.html'
})
export class ProjectDesignComponent {
	@Input() public formGroup: FormGroup;
	constructor() {}
}
