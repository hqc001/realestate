import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
	selector: 'admin-app-project-appendix',
	templateUrl: './project-appendix.component.html'
})
export class ProjectAppendixComponent {
	@Input() public formGroup: FormGroup;
	constructor() {}
}
