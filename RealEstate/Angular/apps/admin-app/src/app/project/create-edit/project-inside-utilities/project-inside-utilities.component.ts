import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
	selector: 'admin-app-project-inside-utilities',
	templateUrl: './project-inside-utilities.component.html'
})
export class ProjectInsideUtilitiesComponent {
	@Input() public formGroup: FormGroup;
	constructor() {}
}
