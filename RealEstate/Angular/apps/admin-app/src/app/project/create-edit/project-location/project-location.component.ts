import { MapsAPILoader, MouseEvent } from '@agm/core';
import { ChangeDetectorRef, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Utilities } from '@codeforfun/admin/utilities';
import { Subscription } from 'rxjs';

@Component({
	selector: 'admin-app-project-location',
	templateUrl: './project-location.component.html',
	styleUrls: ['./project-location.component.scss']
})
export class ProjectLocationComponent implements OnInit, OnDestroy {
	@Input() public formGroup: FormGroup;
	@ViewChild('searchBox') searchBox: ElementRef<HTMLInputElement>;

	public marker = null;
	public latitude = 10.7553411;
	public longitude = 106.4150395;
	public zoom = 10;

	private _subscriptions: Subscription[] = [];

	constructor(private _mapsAPILoader: MapsAPILoader, private _cdr: ChangeDetectorRef) {
		this._mapsAPILoader.load().then(() => {
			const autocomplete = new google.maps.places.Autocomplete(this.searchBox.nativeElement);
			autocomplete.addListener('place_changed', () => {
				// get the place result
				const place = autocomplete.getPlace();

				// verify result
				if (place.geometry === undefined || place.geometry === null) {
					return;
				}

				// set latitude, longitude and zoom
				this.latitude = place.geometry.location.lat();
				this.longitude = place.geometry.location.lng();
				this.zoom = 18;
				this._cdr.detectChanges();
			});
		});
	}

	public ngOnInit() {
		const setData = (data: { lat: number; lng: number }) => {
			if (data && data.lat && data.lng) {
				this.latitude = data.lat;
				this.longitude = data.lng;
				this.zoom = 18;
				this.marker = {
					lat: data.lat,
					lng: data.lng,
					label: 'P'
				};
			}
		};

		setData(this.formGroup.value);

		this._subscriptions.push(
			this.formGroup.valueChanges.subscribe(data => {
				setData(data);
			})
		);
	}

	public ngOnDestroy() {
		Utilities.unsubcribe(this._subscriptions);
	}

	public mapClicked($event: MouseEvent) {
		this.marker = {
			lat: $event.coords.lat,
			lng: $event.coords.lng,
			label: 'P'
		};

		this.formGroup.patchValue(
			{
				lat: $event.coords.lat,
				lng: $event.coords.lng
			},
			{ emitEvent: false }
		);
	}
}
