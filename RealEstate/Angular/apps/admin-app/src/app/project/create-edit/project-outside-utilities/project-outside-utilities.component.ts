import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
	selector: 'admin-app-project-outside-utilities',
	templateUrl: './project-outside-utilities.component.html'
})
export class ProjectOutsideUtilitiesComponent {
	@Input() public formGroup: FormGroup;
	constructor() {}
}
