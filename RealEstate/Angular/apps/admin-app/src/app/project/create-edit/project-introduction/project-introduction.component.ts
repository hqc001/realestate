import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
	selector: 'admin-app-project-introduction',
	templateUrl: './project-introduction.component.html',
	styleUrls: ['./project-introduction.component.scss']
})
export class ProjectIntroductionComponent {
	@Input() public formGroup: FormGroup;
	constructor() {}
}
