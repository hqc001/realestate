import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
	selector: 'admin-app-project-preferential',
	templateUrl: './project-preferential.component.html'
})
export class ProjectPreferentialComponent {
	@Input() public formGroup: FormGroup;
	constructor() {}
}
