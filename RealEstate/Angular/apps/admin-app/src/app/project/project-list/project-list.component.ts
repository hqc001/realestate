import { Component } from '@angular/core';
import { IProjectTableData } from '@codeforfun/admin/models';
import { TableSettings } from '@codeforfun/mat-table';
import { ProjectDataSourceService } from '@codeforfun/admin/table-datasource';
import { DecimalPipe } from '@angular/common';
import { ProjectListFeatureService } from '@codeforfun/admin/services';

@Component({
	selector: 'admin-app-project-list',
	templateUrl: './project-list.component.html',
	styleUrls: ['./project-list.component.scss'],
	providers: [DecimalPipe, ProjectDataSourceService]
})
export class ProjectListComponent {
	public projectTableSetting: TableSettings<IProjectTableData>;

	constructor(
		public projectTableDataSource: ProjectDataSourceService,
		private _projListFtSv: ProjectListFeatureService,
		private _decimalPipe: DecimalPipe
	) {
		this.projectTableSetting = new TableSettings({
			columns: [
				{ columnDef: 'id', header: 'Id', cellData: data => this._decimalPipe.transform(data.id) },
				{ columnDef: 'uid', header: 'UId', cellData: data => data.uid },
				{ columnDef: 'name', header: 'Tên dự án', cellData: data => data.name },
				{ columnDef: 'invesloperName', header: 'Chủ đầu tư', cellData: data => data.invesloperName },
				{ columnDef: 'type', header: 'Loại', cellData: data => data.type },
				{ columnDef: 'block', header: 'Số block', cellData: data => this._decimalPipe.transform(data.block) },
				{ columnDef: 'unit', header: 'Số căn', cellData: data => this._decimalPipe.transform(data.unit) }
			],
			actionButtons: [
				{ name: 'Sửa', iconCss: 'fas fa-edit', color: 'primary', action: this._projListFtSv.onEdit },
				{ name: 'Xóa', iconCss: 'fas fa-trash-alt', color: 'warn', action: this._onDeleteBtnClick }
			]
		});
	}

	public addNew() {
		this._projListFtSv.onAddNew();
	}

	private _onDeleteBtnClick = (data: IProjectTableData) => {
		this._projListFtSv.onDelete(data, this.projectTableDataSource);
	};
}
