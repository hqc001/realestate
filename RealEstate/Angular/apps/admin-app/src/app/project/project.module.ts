import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import {
	MatButtonModule,
	MatCardModule,
	MatDatepickerModule,
	MatFormFieldModule,
	MatInputModule,
	MatSelectModule,
	MatTabsModule,
	MatCheckboxModule
} from '@angular/material';
import { RouterModule } from '@angular/router';
import { AdminAppCommonModule } from '@codeforfun/admin/commons';
import {
	ProjectDetailsFeatureService,
	ProjectDetailsResolveGuard,
	ProjectListFeatureService
} from '@codeforfun/admin/services';
import { AppMatTableModule } from '@codeforfun/mat-table';

/* **************************************
 * All Components
 * **************************************/
import { ProjectAppendixComponent } from './create-edit/project-appendix/project-appendix.component';
import { ProjectDesignComponent } from './create-edit/project-design/project-design.component';
import { ProjectDetailsComponent } from './create-edit/project-details/project-details.component';
import { ProjectInsideUtilitiesComponent } from './create-edit/project-inside-utilities/project-inside-utilities.component';
import { ProjectOutsideUtilitiesComponent } from './create-edit/project-outside-utilities/project-outside-utilities.component';
import { ProjectPreferentialComponent } from './create-edit/project-preferential/project-preferential.component';
import { ProjectComponent } from './create-edit/project.component';
import { ProjectListComponent } from './project-list/project-list.component';
import { ProjectLocationComponent } from './create-edit/project-location/project-location.component';
import { AgmCoreModule } from '@agm/core';
import { ProjectIntroductionComponent } from './create-edit/project-introduction/project-introduction.component';

const route: RouteWithData<AppRouteData>[] = [
	{
		path: '',
		children: [
			{
				path: '',
				component: ProjectListComponent,
				data: {
					title: 'Danh sách dự án'
				}
			},
			{
				path: ':id',
				component: ProjectComponent,
				data: {
					path: 'du-an/:id',
					title: 'Dự án'
				},
				resolve: {
					_: ProjectDetailsResolveGuard
				}
			}
		]
	}
];

@NgModule({
	declarations: [
		ProjectComponent,
		ProjectDetailsComponent,
		ProjectInsideUtilitiesComponent,
		ProjectListComponent,
		ProjectOutsideUtilitiesComponent,
		ProjectDesignComponent,
		ProjectAppendixComponent,
		ProjectPreferentialComponent,
		ProjectLocationComponent,
		ProjectIntroductionComponent
	],
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		AdminAppCommonModule,
		AppMatTableModule,
		MatDatepickerModule,
		MatCardModule,
		MatTabsModule,
		MatButtonModule,
		MatInputModule,
		MatFormFieldModule,
		MatSelectModule,
		MatCheckboxModule,
		AgmCoreModule,
		RouterModule.forChild(route)
	],
	exports: [],
	providers: [ProjectDetailsResolveGuard, ProjectDetailsFeatureService, ProjectListFeatureService],
	schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProjectModule {}
