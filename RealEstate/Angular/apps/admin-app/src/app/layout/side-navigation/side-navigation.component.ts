import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { Utilities } from '@codeforfun/admin/utilities';

@Component({
	selector: 'admin-app-side-navigation',
	templateUrl: './side-navigation.component.html',
	styleUrls: ['./side-navigation.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class SideNavigationComponent {
	public matPanelHeaderHeight = '44px';

	constructor(public router: Router, private _activatedRoute: ActivatedRoute, private _cdr: ChangeDetectorRef) {
		this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(_ => {
			if (!this._cdr['destroyed']) {
				this._cdr.detectChanges();
			}
		});
	}

	public isMatchRoute(path: string) {
		if (!this.router) return false;

		const childRoute = Utilities.getCurrentActivatedRouteChild(this._activatedRoute);
		return childRoute.snapshot.data.path === path;
	}
}
