import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout.component';
import { SideNavigationComponent } from './side-navigation/side-navigation.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SideNavHoverDirective } from './directives/sidenav-hover.directive';
import { SafeHtmlPipe } from '@codeforfun/admin/pipes';

import {
	MatGridListModule,
	MatCardModule,
	MatMenuModule,
	MatIconModule,
	MatButtonModule,
	MatToolbarModule,
	MatSidenavModule,
	MatListModule,
	MatInputModule,
	MatExpansionModule,
	MatRippleModule,
	MatTooltipModule,
	MatIconRegistry,
	MatSnackBarModule,
	MatDialogModule
} from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import { RouterModule } from '@angular/router';
import { NgProgressModule } from '@ngx-progressbar/core';

const layoutComponents = [
	LayoutComponent,
	SideNavigationComponent,
	HeaderComponent,
	FooterComponent,
	SideNavHoverDirective
];

const materialModules = [
	MatGridListModule,
	MatCardModule,
	MatMenuModule,
	MatIconModule,
	MatButtonModule,
	MatToolbarModule,
	MatSidenavModule,
	MatListModule,
	MatInputModule,
	MatExpansionModule,
	MatRippleModule,
	MatTooltipModule,
	MatSnackBarModule,
	MatDialogModule,
	LayoutModule
];

@NgModule({
	declarations: [...layoutComponents, SafeHtmlPipe],
	imports: [CommonModule, RouterModule, NgProgressModule, ...materialModules],
	exports: [...layoutComponents, ...materialModules],
	providers: [MatIconRegistry],
	schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLayoutModule {}
