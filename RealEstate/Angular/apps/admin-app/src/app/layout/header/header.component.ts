import { Component, ChangeDetectionStrategy } from '@angular/core';
import { AuthenticationService, LayoutService } from '@codeforfun/admin/services';

@Component({
	selector: 'admin-app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent {
	constructor(public layoutService: LayoutService, private _authService: AuthenticationService) {}

	public toggleSideNav() {
		this.layoutService.isSideNavMini = !this.layoutService.isSideNavMini;
	}

	public signOut() {
		this._authService.doLogout();
	}
}
