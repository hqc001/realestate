import { Component, ViewChild, ElementRef, AfterViewInit, ChangeDetectionStrategy } from '@angular/core';
import { MatSidenav, MatIconRegistry } from '@angular/material';
import { AuthenticationService, LayoutService } from '@codeforfun/admin/services';
import { HttpClientStateService } from '@codeforfun/http-client';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { filter } from 'rxjs/operators';
import { Utilities } from '@codeforfun/admin/utilities';

@Component({
	selector: 'admin-app-layout',
	templateUrl: './layout.component.html',
	styleUrls: ['./layout.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class LayoutComponent implements AfterViewInit {
	@ViewChild('sideNav')
	private _matSideNav: MatSidenav;

	@ViewChild('matSidenavContainer', { read: ElementRef })
	set sideNavContainer(elr: ElementRef) {
		this._sideNavContainer = elr ? elr.nativeElement : null;
		this._toggleCssClasses();
	}

	@ViewChild('sideNav', { read: ElementRef })
	set sideNav(elr: ElementRef) {
		this._sideNav = elr ? elr.nativeElement : null;
		this._toggleCssClasses();
	}

	private _sideNav: HTMLElement;
	private _sideNavContainer: HTMLElement;
	private _isHandset: boolean;

	constructor(
		public authService: AuthenticationService,
		public layoutService: LayoutService,
		public httpProgressState: HttpClientStateService,
		private _router: Router,
		private _actvRouter: ActivatedRoute,
		private _matIconRegistry: MatIconRegistry
	) {
		this._matIconRegistry.registerFontClassAlias('fontawesome', 'fas');
	}

	public ngAfterViewInit() {
		this.layoutService.isSideNavMini$.subscribe(_ => {
			this._toggleSideNav();
		});

		this.layoutService.isHandset$.subscribe(isHandset => {
			this._isHandset = isHandset;
			this._toggleCssClasses();
		});

		this.authService.isLoggedIn$.subscribe(isLoggedIn => {
			this.layoutService.toogleCssClassesIf(document.body, !isLoggedIn, ['bg-light']);
		});

		this._router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(_ => {
			const curActvRoute = Utilities.getCurrentActivatedRouteChild(this._actvRouter);

			if (curActvRoute && curActvRoute.snapshot.data) {
				this.layoutService.title = curActvRoute.snapshot.data.title || '';
			}
		});
	}

	private _toggleSideNav() {
		if (this._isHandset && this._matSideNav) {
			this._matSideNav.toggle();
		}
	}

	private _toggleCssClasses() {
		if (!this._sideNavContainer || !this._sideNav) {
			return;
		}

		this.layoutService.toogleCssClassesIf(this._sideNavContainer, this._isHandset, ['sidenav-handset']);

		this.layoutService.toogleCssClassesIf(this._sideNav, this._isHandset, ['d-none']);

		this.layoutService.toogleCssClassesIf(this._sideNav, this._isHandset, ['d-block'], 500);
	}
}
