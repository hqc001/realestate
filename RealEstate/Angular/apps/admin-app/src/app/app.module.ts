/* ***********************************
 * Angular core
 * ***********************************/
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { RouterModule, Route, PreloadAllModules } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTabsModule, MatFormFieldModule, MatNativeDateModule } from '@angular/material';

/* ***********************************
 * 3rd parties
 * ***********************************/
import { AppHttpClientModule, HttpErrorHandler, HttpClientProgressInterceptor } from '@codeforfun/http-client';
import { AppMatTableModule } from '@codeforfun/mat-table';
import { NgProgressModule } from '@ngx-progressbar/core';
import { NgProgressHttpModule } from '@ngx-progressbar/http';
import { Ng2ImgMaxModule } from 'ng2-img-max';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

/* ***********************************
 * App components
 * ***********************************/
import { AppLayoutModule } from './layout/layout.module';
import { AppComponent } from './app.component';
import { AuthenticationModule } from './authentication/authentication.module';
import { LoginComponent } from './authentication/login/login.component';

/* ***********************************
 * App services
 * ***********************************/
import { AppHttpErrorHandler, AuthencationGuard, LoginPageGuard } from '@codeforfun/admin/services';
import { environment } from '../environments/environment';
import { SnackbarMessageComponent, ConfirmDialogComponent, AdminAppCommonModule } from './shared/commons';
import { AgmCoreModule } from '@agm/core';

const routes: Route[] = [
	{
		path: '',
		canActivateChild: [AuthencationGuard],
		children: [
			{
				path: '',
				redirectTo: 'chu-dau-tu',
				pathMatch: 'full'
			},
			{
				path: 'chu-dau-tu',
				loadChildren: './invesloper/invesloper.module#InvesloperModule'
			},
			{
				path: 'du-an',
				loadChildren: './project/project.module#ProjectModule'
			},
			{
				path: 'hinh-anh',
				loadChildren: './picture/picture.module#PictureModule'
			}
		]
	},
	{
		path: 'auth',
		canActivateChild: [LoginPageGuard],
		children: [
			{
				path: 'dang-nhap',
				component: LoginComponent
			}
		]
	}
];

export function HttpLoaderFactory(http: HttpClient) {
	return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
	declarations: [AppComponent],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		AuthenticationModule,
		AdminAppCommonModule,
		MatTabsModule,
		MatNativeDateModule,
		AppMatTableModule,
		AppLayoutModule,
		Ng2ImgMaxModule,
		NgProgressModule.forRoot({
			color: '#f44336',
			spinner: false,
			thick: true
		}),
		NgProgressHttpModule,
		AgmCoreModule.forRoot({
			apiKey: 'AIzaSyAtQrX8kwc5oU0rVKCwQIjphM0X8Fb2q4o',
			libraries: ['places']
		}),
		RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
		AppHttpClientModule.forRoot({ baseUrl: environment.baseUrl }),
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: HttpLoaderFactory,
				deps: [HttpClient]
			},
			useDefaultLang: true
		})
	],
	providers: [
		{
			provide: HttpErrorHandler,
			useClass: AppHttpErrorHandler
		},
		{
			provide: HTTP_INTERCEPTORS,
			useClass: HttpClientProgressInterceptor,
			multi: true
		}
	],
	bootstrap: [AppComponent],
	entryComponents: [SnackbarMessageComponent, ConfirmDialogComponent]
})
export class AppModule {}
