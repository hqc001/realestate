import { Component, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IInvesloperDetails } from '@codeforfun/admin/models';
import { InvesloperDetailsFeatureService } from '@codeforfun/admin/services';
import { Utilities } from '@codeforfun/admin/utilities';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'admin-app-invesloper-details',
	templateUrl: './invesloper-details.component.html',
	styleUrls: ['./invesloper-details.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class InvesloperDetailsComponent implements OnDestroy {
	public invlopForm: FormGroup;
	public title = '';

	private _titles = {
		add: 'Thêm mới chủ đầu tư',
		edit: 'Chỉnh sửa thông tin chủ đầu tư'
	};

	constructor(
		private _formBuilder: FormBuilder,
		private _invlopFtSv: InvesloperDetailsFeatureService,
		private _activatedRoute: ActivatedRoute
	) {
		this._onDataChangeEvent();
	}

	public ngOnDestroy() {
		this._invlopFtSv.resetData();
	}

	public saveInvesloper() {
		Utilities.updateValueAndValidity(this.invlopForm);

		if (this.invlopForm.valid) {
			this._invlopFtSv.saveInvesloper(this.invlopForm.getRawValue());
		}
	}

	private _buildForm = (data: IInvesloperDetails) => {
		this.title = this._isEditMode ? this._titles.edit : this._titles.add;

		if (!this.invlopForm) {
			this.invlopForm = Utilities.buildForm<IInvesloperDetails>(this._formBuilder, {
				id: [data.id],
				name: [data.name, Validators.required],
				url: [data.url, Validators.required],
				description: [data.description, Validators.required]
			});
		} else {
			this.invlopForm.reset(data);
		}
	};

	private _onDataChangeEvent() {
		this._invlopFtSv.invesloperDetail$.subscribe(this._buildForm);
	}

	private get _isEditMode() {
		return +this._activatedRoute.snapshot.params['id'] ? true : false;
	}
}
