import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule, MatCardModule, MatButtonModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { EditorModule } from '@tinymce/tinymce-angular';
import { AppMatTableModule } from '@codeforfun/mat-table';
import {
	InvesloperDetailsResolver,
	InvesloperListFeatureService,
	InvesloperDetailsFeatureService
} from '@codeforfun/admin/services';
import { AdminAppCommonModule, SnackbarMessageComponent, ConfirmDialogComponent } from '@codeforfun/admin/commons';
import { InvesloperDetailsComponent } from './invesloper-details/invesloper-details.component';
import { InvesloperListComponent } from './invesloper-list/invesloper-list.component';

const route: RouteWithData<AppRouteData>[] = [
	{
		path: '',
		children: [
			{
				path: '',
				component: InvesloperListComponent,
				data: {
					title: 'Danh sách chủ đầu tư'
				}
			},
			{
				path: ':id',
				component: InvesloperDetailsComponent,
				data: {
					path: 'chu-dau-tu/:id',
					title: 'Chủ đầu tư'
				},
				resolve: {
					_: InvesloperDetailsResolver
				}
			}
		]
	}
];

@NgModule({
	declarations: [InvesloperDetailsComponent, InvesloperListComponent],
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		AdminAppCommonModule,
		MatFormFieldModule,
		MatInputModule,
		MatCardModule,
		MatButtonModule,
		EditorModule,
		AppMatTableModule,
		RouterModule.forChild(route)
	],
	exports: [],
	providers: [InvesloperDetailsResolver, InvesloperListFeatureService, InvesloperDetailsFeatureService]
})
export class InvesloperModule {}
