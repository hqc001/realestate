import { Component, ChangeDetectionStrategy } from '@angular/core';
import { TableSettings } from '@codeforfun/mat-table';
import { InvesloperDataSourceService } from '@codeforfun/admin/table-datasource';
import { IInvesloperTableData } from '@codeforfun/admin/models';
import { InvesloperListFeatureService } from '@codeforfun/admin/services';

@Component({
	selector: 'admin-app-invesloper-list',
	templateUrl: './invesloper-list.component.html',
	styleUrls: ['./invesloper-list.component.scss'],
	providers: [InvesloperDataSourceService],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class InvesloperListComponent {
	public invesloperTableSetting: TableSettings<IInvesloperTableData>;

	constructor(
		public invesloperDataSource: InvesloperDataSourceService,
		private _invesloperFeatureService: InvesloperListFeatureService
	) {
		this.invesloperTableSetting = new TableSettings({
			columns: [
				{ columnDef: 'id', header: 'Id', cellData: data => data.id },
				{ columnDef: 'name', header: 'Tên nhà đầu tư', cellData: data => data.name },
				{ columnDef: 'url', header: 'Website', cellData: data => data.url }
			],
			actionButtons: [
				{ name: 'Sửa', iconCss: 'fas fa-edit', color: 'primary', action: this._onEditBtnClick },
				{ name: 'Xóa', iconCss: 'fas fa-trash-alt', color: 'warn', action: this._onDeleteBtnClick }
			]
		});
	}

	public addNew() {
		this._invesloperFeatureService.onAddNew();
	}

	private _onEditBtnClick = (data: IInvesloperTableData) => {
		this._invesloperFeatureService.onEditBtnClick(data);
	};

	private _onDeleteBtnClick = (data: IInvesloperTableData) => {
		this._invesloperFeatureService.onDeleteBtnClick(data, this.invesloperDataSource);
	};
}
