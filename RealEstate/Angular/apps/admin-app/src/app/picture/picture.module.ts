import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule, MatProgressSpinnerModule, MatSelectModule } from '@angular/material';
import { AdminAppCommonModule } from '@codeforfun/admin/commons';
import { PictureComponent } from './picture.component';
import { PictureApi } from './services/picture.api';
import { PictureListComponent } from './picture-list/picture-list.component';
import { PictureDataSourceService } from '../shared/table-datasource/picture/picture-datasource';
import { AppMatTableModule } from '@codeforfun/mat-table';
import { RouterModule } from '@angular/router';
import { PictureDetailFeatureService } from '../shared/services/features/picture/picture-detail-feature.service';
import { PictureListFeatureService } from '../shared/services/features/picture/picture-list-feature.service';

const route: RouteWithData<AppRouteData>[] = [
	{
		path: '',
		children: [
			{
				path: '',
				component: PictureListComponent,
				data: {
					title: 'Danh sách hình ảnh'
				}
			},
			{
				path: 'up-hinh',
				component: PictureComponent,
				data: {
					title: 'Upload hình ảnh'
				}
			}
		]
	}
];

@NgModule({
	imports: [
		CommonModule,
		ReactiveFormsModule,
		FormsModule,
		MatProgressSpinnerModule,
		MatFormFieldModule,
		MatSelectModule,
		MatInputModule,
		AdminAppCommonModule,
		AppMatTableModule,
		RouterModule.forChild(route)
	],
	declarations: [PictureComponent, PictureListComponent],
	exports: [PictureComponent],
	providers: [PictureApi, PictureDetailFeatureService, PictureDataSourceService, PictureListFeatureService],
	schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PictureModule {}
