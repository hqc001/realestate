import { Component, OnInit } from '@angular/core';
import { IPictureTableData } from '../../shared/models/picture/picture.model';
import { TableSettings } from '@codeforfun/mat-table';
import { PictureDataSourceService } from '../../shared/table-datasource/picture/picture-datasource';
import { PictureDetailFeatureService } from '../../shared/services/features/picture/picture-detail-feature.service';
import { PictureListFeatureService } from '../../shared/services/features/picture/picture-list-feature.service';
import { environment } from '../../../environments/environment';
import { SnackBarMessageService } from '@codeforfun/admin/services';

@Component({
	selector: 'admin-app-picture-list',
	templateUrl: './picture-list.component.html',
	styleUrls: ['./picture-list.component.scss']
})
export class PictureListComponent implements OnInit {
	pictureTableSetting: TableSettings<IPictureTableData>;
	constructor(
		private _pictureSv: PictureDetailFeatureService,
		private _picListFtSv: PictureListFeatureService,
		private _snackMessage: SnackBarMessageService,
		public pictureDataSourceTableSv: PictureDataSourceService
	) {
		this.pictureTableSetting = new TableSettings({
			columns: [
				{ columnDef: 'id', header: 'Id', cellData: data => data.id },
				{ columnDef: 'name', header: 'Tên', cellData: data => data.name },
				{ columnDef: 'projectName', header: 'Tên dự án', cellData: data => data.projectName },
				{ columnDef: 'image', header: 'Hình ảnh', isCustomColDef: true }
			],
			actionButtons: [
				{ name: 'Copy', iconCss: 'fas fa-copy', color: 'primary', action: this._onCopyBtnClick },
				{ name: 'Xóa', iconCss: 'fas fa-trash-alt', color: 'warn', action: this._onDeleteBtnClick }
			]
		});
	}

	public ngOnInit() {}

	public getThumbnaiImage(thumbnailName: string) {
		return environment.uploadUrl + thumbnailName;
	}

	public addNew() {
		this._picListFtSv.onAddNew();
	}
	private _onDeleteBtnClick = (data: IPictureTableData) => {
		this._picListFtSv.onDelete(data, this.pictureDataSourceTableSv);
	};

	private _onCopyBtnClick = (data: IPictureTableData) => {
		const selBox = document.createElement('textarea');
		selBox.value = environment.uploadUrl + data.name;
		document.body.appendChild(selBox);
		selBox.focus();
		selBox.select();
		document.execCommand('copy');
		document.body.removeChild(selBox);
		this._snackMessage.showMessage('Copied !', 'success');
	};
}
