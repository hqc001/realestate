import { Body, HttpAbstractService, POST, GET, ApiSettings, Query, DELETE, ResponeTypes } from '@codeforfun/http-client';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { IPictureTableData } from '../../shared/models/picture/picture.model';
import { ITableDataSource, ITableParameter } from '@codeforfun/mat-table';

@Injectable()
export class PictureApi extends HttpAbstractService {
	
	@DELETE('api/image')
	@ApiSettings({ responeType: ResponeTypes.Text })
	public deletePicture(@Query('image') params: IPictureTableData): Observable<string> {
		return null;
	}

	@POST('api/upload/files')
	public doUpload(@Body fd: FormData): Observable<any> {
		return null;
	}

	@GET('api/project/list')
	public getProjects(): Observable<any> {
		return null;
	}
	
	@GET('api/image/table')
	public getPictureTableData(@Query('') params: ITableParameter): Observable<ITableDataSource<IPictureTableData>> {
		return null;
	}

}
