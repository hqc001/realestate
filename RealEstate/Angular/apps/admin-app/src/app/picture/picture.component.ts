import { Component, OnInit } from '@angular/core';
import { Ng2ImgMaxService } from 'ng2-img-max';
import { IProject } from '../shared/models/project/project.model';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { PictureDetailFeatureService } from '../shared/services/features/picture/picture-detail-feature.service';

@Component({
	selector: 'admin-app-picture',
	templateUrl: './picture.component.html',
	styleUrls: ['./picture.component.scss']
})
export class PictureComponent {
	selectedId;
	projects: Array<IProject>;
	loading = false;
	files: Array<File> = [];
	public pictureForm: FormGroup;

	constructor(public pictureService: PictureDetailFeatureService, private ng2ImgMax: Ng2ImgMaxService, private _formBuilder: FormBuilder) {
		pictureService.getProjects();
		this.pictureForm = new FormGroup({
			projectId : new FormControl(null, Validators.required)
		});
		
	}

	onFileUploadChange(event) {
		this.loading = true;
		console.log(event);
		this.files = [];
		const filesToUpload = <File[]>[];
		for (let y = 0; y < event.target.files.length; y++) {
			filesToUpload.push(event.target.files[y]);
		}

		this.ng2ImgMax.resize(filesToUpload, 1024, 768).subscribe(
			result => {
				this.files.push(new File([result], result.name));
				if (this.files.length === filesToUpload.length) {
					this.loading = false;
				}
			},
			error => {
				console.log('😢 Oh no!', error);
			},
			() => console.log('onComplete')
		);

		// for (let i = 0; i < event.target.files.length; i++) {
		// 	const file = event.target.files[i];
		// 	if (file.type.match(/image.*/)) {
		// 		this.ng2ImgMax.resizeImage(file, 1024, 768).subscribe(
		// 			result => {
		// 				this.files.push(new File([result], result.name));
		// 			},
		// 			error => {
		// 				console.log('😢 Oh no!', error);
		// 			},
		// 			() => console.log('onComplete')
		// 		);
		// 	}
		// }
	}

	onUpload() {
		console.log('start upload');
		if (this.files.length > 0) {
			const fd = new FormData();
			for (let i = 0; i < this.files.length; i++) {
				fd.append('uploads[]', this.files[i], this.files[i]['name']);
			}
			fd.append('projectId', this.pictureForm.get('projectId').value);
			this.pictureService.doUpload(fd);
		}
	}
}
