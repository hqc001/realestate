import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AppFeatureService } from './shared/services/features/app/app-feature.service';

@Component({
	selector: 'admin-app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent {
	constructor(private _translate: TranslateService, private _appFtSv: AppFeatureService) {
		_translate.setDefaultLang('vn');
		_appFtSv.getCitiesAndDistricts();
	}
}
