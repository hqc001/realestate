export interface IUser {
	userName: string;
	roles: IRole[];
}

export interface IRole {
	name: string;
}
