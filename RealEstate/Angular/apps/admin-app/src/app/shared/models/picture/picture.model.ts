export interface IPictureTableData {
	id: number;
	projectName: string;
	name: string;
	thumbnailName: string;
}