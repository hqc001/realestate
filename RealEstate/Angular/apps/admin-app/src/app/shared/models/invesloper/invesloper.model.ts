export interface IInvesloperTableData {
	id: number;
	name: string;
	url: string;
}

export interface IInvesloperDetails extends IInvesloperTableData {
	description: string;
}
