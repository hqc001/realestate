import { IPictureTableData } from '../picture/picture.model';

export interface IProject {
	id: number;
	uid: string;
	name: string;
	area: string;
	block: number;
	city: string;
	code: string;
	densityOfBuilding: number;
	district: string;
	floor: string;
	handoverDate: Date;
	penthouse: string;
	price: string;
	startDate: Date;
	statusId: number;
	type: string;
	unit: number;
	lat: number;
	lng: number;
	address: string;
	invesloperId: number;
	insideUtilities: string;
	outsideUtilities: string;
	design: string;
	appendix: string;
	preferential: string;
	displayImageId?: number;
	isGoodPrice: boolean;
	locationDescription: string;
	introduction: string;
}

export interface IProjectTableData {
	id: number;
	uid: string;
	name: string;
	type: string;
	invesloperName: string;
	block: number;
	unit: number;
}

export interface IProjectDetailsForm {
	projectDetails: any;
	projectInsideUtilities: any;
	projectOutsideUtilities: any;
	projectDesign: any;
	projectAppendix: any;
	projectPreferential: any;
	projectLocation: any;
	projectIntroduction: any;
}

export enum ProjectStatuses {
	Closed = 1,
	Opened = 2,
	ComingSoon = 3
}

export enum ProjectTypes {
	Housing = 1
}

export interface IProjectCreationFormData {
	projectStatuses: SelectOption<ProjectStatuses>[];
	projectInveslopers: SelectOption<number>[];
	projectTypes: SelectOption<ProjectTypes>[];
	projectImages: SelectOption<number>[];
	project: IProject;
}

export interface IProjectDropDownList {
	items: SelectOption<number>;
}

export interface SelectOption<TValue> {
	value: TValue;
	description: string;
}

export interface ICitiesAndDistricts {
	[key: string]: {
		name: string;
		cities: {
			[key: string]: string;
		};
	};
}
