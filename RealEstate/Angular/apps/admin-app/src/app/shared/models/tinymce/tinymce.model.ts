import { Settings } from 'tinymce';

export interface TinyMCESettings extends Settings {
	codesample_content_css?: string;
	autoresize_max_height?: number;
	autoresize_min_height?: number;
}
