import { Route } from '@angular/router';
import { FormGroup } from '@angular/forms';

declare global {
	type FormGroupData<K> = { [P in keyof K]?: any[] | FormGroup };

	interface RouteWithData<T> extends Route {
		data?: T;
		children?: RouteWithData<T>[];
	}

	interface AppRouteData {
		path?: string;
		title: string;
		[k: string]: any;
	}
}
