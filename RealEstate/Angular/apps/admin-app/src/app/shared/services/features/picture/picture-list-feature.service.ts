import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { DialogService, SnackBarMessageService } from '../..';
import { PictureApi } from 'apps/admin-app/src/app/picture/services/picture.api';
import { IPictureTableData } from '../../../models/picture/picture.model';
import { PictureDataSourceService } from '../../../table-datasource/picture/picture-datasource';

@Injectable()
export class PictureListFeatureService {
    constructor(
		private _router: Router,
		private _picApi: PictureApi,
		private _dialogService: DialogService,
		private _snackBarService: SnackBarMessageService
	) {}

	public onAddNew = () => {
		this._router.navigate(['hinh-anh/up-hinh']);
	};

	public onDelete(data: IPictureTableData, dataSource: PictureDataSourceService) {
		this._dialogService
			.showConfirmDialog('Xác nhận xóa', `Bạn có muốn xóa ${data.name} không ?`)
			.subscribe(confirm => {
				if (confirm) {
					this._picApi.deletePicture(data).subscribe(rs => {
						this._snackBarService.showMessage(rs, 'success');
						dataSource.loadFirstPage();
					});
				}
			});
	}
}