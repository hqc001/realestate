import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { SelectOption, ICitiesAndDistricts } from '@codeforfun/admin/models';
import { ProjectApiService } from '../../api/project.api';

@Injectable({
	providedIn: 'root'
})
export class AppFeatureService {
	private _cities$ = new BehaviorSubject<SelectOption<string>[]>(null);
	private _districts$ = new BehaviorSubject<SelectOption<string>[]>(null);
	private _citiesAndDistricts$ = new BehaviorSubject<ICitiesAndDistricts>(null);

	public cities$ = this._cities$.asObservable();
	public districts$ = this._districts$.asObservable();

	constructor(private _projectApi: ProjectApiService) {}

	public getCitiesAndDistricts() {
		this._projectApi.getCitiesAndDistricts().subscribe(data => {
			this._citiesAndDistricts$.next(data);
			const cities: SelectOption<string>[] = [];
			for (const key in data) {
				if (data.hasOwnProperty(key)) {
					cities.push({
						value: key,
						description: data[key].name
					});
				}
			}

			this._cities$.next(cities);
		});
	}

	public getDistrictsByCityKey(cityKey: string) {
		const data = this._citiesAndDistricts$.value || {};
		const districts: SelectOption<string>[] = [];

		const distrs = data[cityKey] ? data[cityKey].cities || {} : {};
		for (const distrKey in distrs) {
			if (distrs.hasOwnProperty(distrKey)) {
				districts.push({
					value: distrKey,
					description: distrs[distrKey]
				});
			}
		}

		this._districts$.next(districts);
	}

	public resetDistricts() {
		this._districts$.next(null);
	}
}
