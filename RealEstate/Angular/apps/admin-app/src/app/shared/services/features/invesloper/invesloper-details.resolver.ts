import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { IInvesloperDetails } from '@codeforfun/admin/models';
import { InvesloperDetailsFeatureService } from './invesloper-details-feature.service';

@Injectable()
export class InvesloperDetailsResolver implements Resolve<IInvesloperDetails> {
	constructor(private _invlopFtSv: InvesloperDetailsFeatureService) {}

	public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
		if (+route.params['id']) {
			this._invlopFtSv.getInvesloperDetails(route.params['id']);
		}

		return true;
	}
}
