import { Injectable, Optional } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { ILoginForm, IUser } from '@codeforfun/admin/models';
import { AuthenticationApiService } from '../../api/authentication.api';

@Injectable({
	providedIn: 'root'
})
export class AuthenticationService {
	private _currentUser$ = new BehaviorSubject<IUser>(null);
	private _isLoggedIn$ = new BehaviorSubject<boolean>(false);

	constructor(private _authApi: AuthenticationApiService, private _router: Router) {}

	public get currentUser$(): Observable<IUser> {
		return this._currentUser$.asObservable();
	}

	public get isLoggedIn$(): Observable<boolean> {
		return this._isLoggedIn$.asObservable();
	}

	public setCurrentUser(user: IUser) {
		this._currentUser$.next(user);
		this._isLoggedIn$.next(user != null);
	}

	public doLogin(loginForm: ILoginForm) {
		this._authApi.login(loginForm).subscribe(() => {
			this._authApi.getUserInfo().subscribe(userInfo => {
				this.setCurrentUser(userInfo);
				this._router.navigate(['/']);
			});
		});
	}

	public doLogout() {
		this._authApi.logout().subscribe(() => {
			this.setCurrentUser(null);
			this._router.navigate(['auth/dang-nhap']);
		});
	}
}
