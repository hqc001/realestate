import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { IProjectCreationFormData } from '@codeforfun/admin/models';
import { ProjectDetailsFeatureService } from './project-detail-feature.service';

@Injectable()
export class ProjectDetailsResolveGuard implements Resolve<IProjectCreationFormData> {
	constructor(private _projDtFtSv: ProjectDetailsFeatureService) {}

	public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
		if (route.params['id'] != null) {
			this._projDtFtSv.getProjectFormData(route.params['id']);
		}
		return true;
	}
}
