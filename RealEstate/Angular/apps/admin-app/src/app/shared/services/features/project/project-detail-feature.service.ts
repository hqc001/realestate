import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { IProjectCreationFormData, IProject, IProjectDetailsForm } from '@codeforfun/admin/models';
import { ProjectApiService } from '../../api/project.api';
import { SnackBarMessageService } from '../snackbar-message/snackbar-message.service';
import { Router } from '@angular/router';

@Injectable()
export class ProjectDetailsFeatureService {
	private _initTabErrorState = {
		projectDetails: false,
		projectInsideUtilities: false,
		projectOutsideUtilities: false,
		projectDesign: false,
		projectAppendix: false,
		projectPreferential: false,
		projectLocation: false,
		projectIntroduction: false
	};

	private _initData: IProject = {
		id: 0,
		invesloperId: null,
		appendix: null,
		area: null,
		block: null,
		city: null,
		code: null,
		densityOfBuilding: null,
		design: null,
		district: null,
		floor: null,
		handoverDate: null,
		insideUtilities: null,
		lat: 0,
		lng: 0,
		address: null,
		name: null,
		outsideUtilities: null,
		penthouse: null,
		preferential: null,
		price: null,
		startDate: null,
		statusId: null,
		type: null,
		uid: null,
		unit: null,
		displayImageId: null,
		isGoodPrice: false,
		locationDescription: null,
		introduction: null
	};

	private _initFormData: IProjectCreationFormData = {
		projectInveslopers: null,
		projectStatuses: null,
		projectTypes: null,
		projectImages: null,
		project: this._initData
	};

	private _formData$ = new BehaviorSubject<IProjectCreationFormData>(this._initFormData);
	private _tabErrorState$ = new BehaviorSubject<IProjectDetailsForm>(this._initTabErrorState);

	public formData$ = this._formData$.asObservable();
	public tabErrorState$ = this._tabErrorState$.asObservable();

	constructor(
		private _projectApi: ProjectApiService,
		private _snackbarService: SnackBarMessageService,
		private _router: Router
	) {}

	public getProjectFormData(id: number) {
		this._projectApi.getProjectFormData(id).subscribe(data => {
			this._formData$.next({
				...data,
				project: data.project ? data.project : this._initData
			});
		});
	}

	public setTabErrorState(key: keyof IProjectDetailsForm | any, state: boolean) {
		this._tabErrorState$.next({
			...this._tabErrorState$.value,
			[key]: state
		});
	}

	public resetTabErrorState() {
		this._tabErrorState$.next(this._initTabErrorState);
	}

	public get tabErrorState() {
		return this._tabErrorState$.value;
	}

	public resetFormData() {
		this._formData$.next(this._initFormData);
	}

	public createOrUpdateProject(proj: IProject) {
		if (proj.id > 0) {
			this._projectApi.updateProject(proj).subscribe(rs => {
				this._snackbarService.showMessage(rs.message, 'success');
				this.getProjectFormData(rs.id);
			});
		} else {
			this._projectApi.createProject(proj).subscribe(rs => {
				this._snackbarService.showMessage(rs.message, 'success');
				this._router.navigate(['du-an', rs.id]);
			});
		}
	}
}
