import { Injectable } from '@angular/core';
import { IInvesloperDetails, IInvesloperTableData } from '@codeforfun/admin/models';
import {
	GET,
	HttpAbstractService,
	Query,
	Body,
	POST,
	PUT,
	DELETE,
	ApiSettings,
	ResponeTypes
} from '@codeforfun/http-client';
import { ITableParameter, ITableDataSource } from '@codeforfun/mat-table';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class InvesloperApiService extends HttpAbstractService {
	@GET('api/invesloper/list')
	public getInveslopers(@Query('') params: ITableParameter): Observable<ITableDataSource<IInvesloperTableData>> {
		return null;
	}

	@GET('api/invesloper')
	public getInvesloperDetails(@Query('id') id: number): Observable<IInvesloperDetails> {
		return null;
	}

	@POST('api/invesloper')
	public createInvesloper(@Body formData: IInvesloperDetails): Observable<number> {
		return null;
	}

	@PUT('api/invesloper')
	public updateInvesloper(@Body formData: IInvesloperDetails): Observable<number> {
		return null;
	}

	@DELETE('api/invesloper')
	@ApiSettings({ responeType: ResponeTypes.Text })
	public deleteInvesloper(@Query('id') id: number): Observable<string> {
		return null;
	}
}
