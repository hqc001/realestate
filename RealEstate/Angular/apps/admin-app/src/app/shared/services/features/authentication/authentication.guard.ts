import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivateChild, RouterStateSnapshot, Router, CanActivate } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { AuthenticationApiService } from '../../api/authentication.api';
import { AuthenticationService } from './authentication.service';

@Injectable({
	providedIn: 'root'
})
export class AuthencationGuard implements CanActivateChild, CanActivate {
	constructor(
		private _authApi: AuthenticationApiService,
		private _authService: AuthenticationService,
		private _router: Router
	) {}

	public canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
		return this._authService.isLoggedIn$.pipe(
			switchMap(isLoggedIn => {
				if (isLoggedIn) {
					return of(isLoggedIn);
				} else {
					return this._authApi.getUserInfo().pipe(
						switchMap(data => {
							this._authService.setCurrentUser(data);
							return of(true);
						}),
						catchError(_ => {
							this._router.navigate(['auth/dang-nhap']);
							return of(false);
						})
					);
				}
			})
		);
	}

	public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
		return this.canActivateChild(route, state);
	}
}
