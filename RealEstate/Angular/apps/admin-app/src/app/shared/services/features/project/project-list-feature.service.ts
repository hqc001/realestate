import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { IProjectTableData } from '@codeforfun/admin/models';
import { ProjectDataSourceService } from '@codeforfun/admin/table-datasource';
import { DialogService } from '../dialog/dialog.service';
import { SnackBarMessageService } from '../snackbar-message/snackbar-message.service';
import { ProjectApiService } from '../../api/project.api';

@Injectable()
export class ProjectListFeatureService {
	constructor(
		private _router: Router,
		private _projectApi: ProjectApiService,
		private _dialogService: DialogService,
		private _snackBarService: SnackBarMessageService
	) {}

	public onAddNew = () => {
		this._router.navigate(['du-an', '0']);
	};

	public onEdit = (data: IProjectTableData) => {
		this._router.navigate(['du-an', data.id]);
	};

	public onDelete(data: IProjectTableData, dataSource: ProjectDataSourceService) {
		this._dialogService
			.showConfirmDialog('Xác nhận xóa', `Bạn có muốn xóa ${data.name} không ?`)
			.subscribe(confirm => {
				if (confirm) {
					this._projectApi.deleteProject(data.id).subscribe(rs => {
						this._snackBarService.showMessage(rs, 'success');
						dataSource.loadFirstPage();
					});
				}
			});
	}
}
