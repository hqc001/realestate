import { Injectable } from '@angular/core';
import { IProjectCreationFormData, ICitiesAndDistricts, IProject, IProjectTableData } from '@codeforfun/admin/models';
import {
	GET,
	HttpAbstractService,
	POST,
	PUT,
	Body,
	Query,
	DELETE,
	ApiSettings,
	ResponeTypes
} from '@codeforfun/http-client';
import { Observable } from 'rxjs';
import { ITableParameter, ITableDataSource } from '@codeforfun/mat-table';

@Injectable({
	providedIn: 'root'
})
export class ProjectApiService extends HttpAbstractService {
	@GET('assets/data/cities.json')
	public getCitiesAndDistricts(): Observable<ICitiesAndDistricts> {
		return null;
	}

	@GET('api/project/form')
	public getProjectFormData(@Query('id') id: number): Observable<IProjectCreationFormData> {
		return null;
	}

	@GET('api/project/table')
	public getProjectTableData(@Query('') params: ITableParameter): Observable<ITableDataSource<IProjectTableData>> {
		return null;
	}

	@POST('api/project')
	public createProject(@Body project: IProject): Observable<{ id: number; message: string }> {
		return null;
	}

	@PUT('api/project')
	public updateProject(@Body project: IProject): Observable<{ id: number; message: string }> {
		return null;
	}

	@DELETE('api/project')
	@ApiSettings({ responeType: ResponeTypes.Text })
	public deleteProject(@Query('id') id: number): Observable<string> {
		return null;
	}
}
