import { Injectable } from '@angular/core';
import { IInvesloperDetails } from '@codeforfun/admin/models';
import { BehaviorSubject } from 'rxjs';
import { InvesloperApiService } from '../../api/invesloper.api';
import { SnackBarMessageService } from '../snackbar-message/snackbar-message.service';
import { Router } from '@angular/router';

@Injectable()
export class InvesloperDetailsFeatureService {
	private _initData: IInvesloperDetails = {
		id: 0,
		description: '',
		name: '',
		url: ''
	};

	private _invesloperDetail$ = new BehaviorSubject<IInvesloperDetails>(this._initData);

	constructor(
		private _invlopApi: InvesloperApiService,
		private _snackBarMessage: SnackBarMessageService,
		private _router: Router
	) {}

	public get invesloperDetail$() {
		return this._invesloperDetail$.asObservable();
	}

	public setInvesloperDetail(data: IInvesloperDetails) {
		this._invesloperDetail$.next(data);
	}

	public getInvesloperDetails = (id: number) => {
		this._invlopApi.getInvesloperDetails(id).subscribe(data => {
			this.setInvesloperDetail(data);
		});
	};

	public saveInvesloper(formData: IInvesloperDetails) {
		if (formData.id) {
			this._invlopApi.updateInvesloper(formData).subscribe(id => {
				this.getInvesloperDetails(id);
				this._snackBarMessage.showMessage('Cập nhật thành công', 'success');
			});
		} else {
			this._invlopApi.createInvesloper(formData).subscribe(id => {
				this._snackBarMessage.showMessage('Tạo mới thành công', 'success');
				this._router.navigate(['chu-dau-tu', id]);
			});
		}
	}

	public resetData() {
		this._invesloperDetail$.next(this._initData);
	}
}
