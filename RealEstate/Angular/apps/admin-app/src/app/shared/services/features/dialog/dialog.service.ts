import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../../../commons/confirm-dialog/confirm-dialog.component';
import { Observable, Subject } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class DialogService {
	constructor(private _matDialog: MatDialog) {}

	public showConfirmDialog(title: string, message: string): Observable<boolean> {
		const result = new Subject<boolean>();
		const dialogRef = this._matDialog.open(ConfirmDialogComponent, {
			data: {
				title,
				message
			}
		});

		dialogRef.afterClosed().subscribe(confirm => {
			result.next(confirm);
			result.complete();
		});

		return result.asObservable();
	}
}
