import { Injectable } from '@angular/core';
import { CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthenticationService } from './authentication.service';

@Injectable({
	providedIn: 'root'
})
export class LoginPageGuard implements CanActivateChild {
	constructor(private _authService: AuthenticationService) {}

	public canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
		return this._authService.isLoggedIn$.pipe(map(isLoggedIn => !isLoggedIn));
	}
}
