import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { IInvesloperTableData } from '@codeforfun/admin/models';
import { InvesloperApiService } from '../../api/invesloper.api';
import { DialogService } from '../dialog/dialog.service';
import { InvesloperDataSourceService } from '@codeforfun/admin/table-datasource';
import { SnackBarMessageService } from '../snackbar-message/snackbar-message.service';

@Injectable()
export class InvesloperListFeatureService {
	constructor(
		private _router: Router,
		private _invesloperApi: InvesloperApiService,
		private _dialogService: DialogService,
		private _snackBarService: SnackBarMessageService
	) {}

	public onAddNew() {
		this._router.navigate(['chu-dau-tu', '0']);
	}

	public onEditBtnClick(data: IInvesloperTableData) {
		this._router.navigate(['chu-dau-tu', data.id]);
	}

	public onDeleteBtnClick(data: IInvesloperTableData, dataSource: InvesloperDataSourceService) {
		this._dialogService
			.showConfirmDialog('Xác nhận xóa', `Bạn có muốn xóa ${data.name} không ?`)
			.subscribe(confirm => {
				if (confirm) {
					this._invesloperApi.deleteInvesloper(data.id).subscribe(rs => {
						this._snackBarService.showMessage(rs, 'success');
						dataSource.loadFirstPage();
					});
				}
			});
	}
}
