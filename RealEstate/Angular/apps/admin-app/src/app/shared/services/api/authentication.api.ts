import { Injectable } from '@angular/core';
import { Body, HttpAbstractService, POST, GET, ApiSettings } from '@codeforfun/http-client';
import { Observable } from 'rxjs';
import { ILoginForm, IUser } from '@codeforfun/admin/models';

@Injectable({
	providedIn: 'root'
})
export class AuthenticationApiService extends HttpAbstractService {
	@POST('api/authentication/login')
	public login(@Body loginForm: ILoginForm): Observable<any> {
		return null;
	}

	@POST('api/authentication/logout')
	public logout(): Observable<any> {
		return null;
	}

	@GET('api/authentication')
	@ApiSettings({ handleError: false })
	public getUserInfo(): Observable<IUser> {
		return null;
	}
}
