import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import * as HttpStatus from 'http-status-codes';
import { HttpErrorHandler } from '@codeforfun/http-client';
import { AuthenticationService } from '../features/authentication/authentication.service';
import { SnackBarMessageService } from '../features/snackbar-message/snackbar-message.service';

@Injectable()
export class AppHttpErrorHandler implements HttpErrorHandler {
	constructor(
		private _router: Router,
		private _authService: AuthenticationService,
		private _snackBarService: SnackBarMessageService
	) {}

	public handleError(error: HttpErrorResponse) {
		switch (error.status) {
			case HttpStatus.UNAUTHORIZED:
			case HttpStatus.FORBIDDEN: {
				this._authService.setCurrentUser(null);
				this._router.navigate(['auth/dang-nhap']);
				break;
			}
			case HttpStatus.BAD_REQUEST:
			case HttpStatus.NOT_FOUND: {
				this._snackBarService.showMessage(error.error, 'error');
				break;
			}
		}

		return throwError(error);
	}
}
