import { Injectable } from '@angular/core';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { map, delay } from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class LayoutService {
	constructor(private _breakpointObserver: BreakpointObserver) {}

	private _title$ = new BehaviorSubject('');
	private _isSideNavMini$ = new BehaviorSubject(true);

	public title$ = this._title$.asObservable();
	public isSideNavMini$ = this._isSideNavMini$.asObservable();
	public isHandset$ = this._breakpointObserver.observe(Breakpoints.Handset).pipe(map(result => result.matches));

	public get title() {
		return this._title$.value;
	}

	public set title(title: string) {
		this._title$.next(title);
	}

	public get isSideNavMini() {
		return this._isSideNavMini$.value;
	}

	public set isSideNavMini(val: boolean) {
		this._isSideNavMini$.next(val);
	}

	public toogleCssClassesIf(element: HTMLElement, isAdd: boolean, cssClasses: string[], timeout = 0) {
		const toggleClasses = () => {
			if (isAdd) {
				element.classList.add(...cssClasses);
			} else {
				element.classList.remove(...cssClasses);
			}
		};

		if (timeout > 0) {
			setTimeout(() => {
				toggleClasses();
			}, timeout);
		} else {
			toggleClasses();
		}
	}
}
