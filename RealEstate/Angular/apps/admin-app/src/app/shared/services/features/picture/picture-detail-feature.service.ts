import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { SnackBarMessageService } from '@codeforfun/admin/services';
import { PictureApi } from 'apps/admin-app/src/app/picture/services/picture.api';
import { IProjectDropDownList } from '@codeforfun/admin/models';

@Injectable()
export class PictureDetailFeatureService {
	constructor(private api: PictureApi, private _snackbarService: SnackBarMessageService) {}

	private _initData = {
		items: null
	};

	private _projects$ = new BehaviorSubject<IProjectDropDownList>(this._initData);
	public projects$ = this._projects$.asObservable();

	doUpload(fd: FormData) {
		this.api.doUpload(fd).subscribe(result => {
			this._snackbarService.showMessage('Files Uploaded', 'success');
			console.log(result);
		});
	}

	getProjects() {
		this.api.getProjects().subscribe(data => {
			this._projects$.next({
				...data
			});
		});
	}
}
