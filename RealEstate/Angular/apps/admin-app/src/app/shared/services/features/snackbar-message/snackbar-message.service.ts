import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { SnackbarMessageComponent } from '../../../commons/snackbar-message/snackbar-message.component';

@Injectable({
	providedIn: 'root'
})
export class SnackBarMessageService {
	constructor(private _snackBar: MatSnackBar) {}

	public showMessage(message: string, type: 'success' | 'error') {
		this._snackBar.openFromComponent(SnackbarMessageComponent, {
			data: {
				message,
				type
			},
			duration: 3000,
			horizontalPosition: 'right',
			verticalPosition: 'bottom',
			panelClass: [type === 'success' ? 'snackbar-message-success' : 'snackbar-message-error']
		});
	}
}
