import { Injectable } from '@angular/core';
import { TableDataSource, UpdateDataSource } from '@codeforfun/mat-table';
import { ProjectApiService } from '../../services/api/project.api';
import { IProjectTableData } from '@codeforfun/admin/models';

@Injectable()
export class ProjectDataSourceService extends TableDataSource<IProjectTableData> {
	constructor(private _projectApi: ProjectApiService) {
		super();
	}

	@UpdateDataSource()
	public load() {
		return this._projectApi.getProjectTableData(this.params);
	}
}
