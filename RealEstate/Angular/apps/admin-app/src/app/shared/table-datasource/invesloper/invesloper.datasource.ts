import { Injectable } from '@angular/core';
import { TableDataSource, UpdateDataSource } from '@codeforfun/mat-table';
import { IInvesloperTableData } from '@codeforfun/admin/models';
import { InvesloperApiService } from '@codeforfun/admin/services';

@Injectable()
export class InvesloperDataSourceService extends TableDataSource<IInvesloperTableData> {
	constructor(private _invesloperApi: InvesloperApiService) {
		super();
	}

	@UpdateDataSource()
	public load() {
		return this._invesloperApi.getInveslopers(this.params);
	}
}
