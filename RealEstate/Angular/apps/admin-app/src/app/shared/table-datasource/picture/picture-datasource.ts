import { Injectable } from "@angular/core";
import { TableDataSource, ITableDataSource, UpdateDataSource } from '@codeforfun/mat-table';
import { IPictureTableData } from '../../models/picture/picture.model';
import { PictureApi } from '../../../picture/services/picture.api';

@Injectable()
export class PictureDataSourceService extends TableDataSource<IPictureTableData> {
    constructor(private _api: PictureApi){
        super();
    }
    
	@UpdateDataSource()
    public load() {
        return this._api.getPictureTableData(this.params);
    }
    
}