import {
	Component,
	OnDestroy,
	Self,
	Optional,
	Input,
	AfterViewInit,
	HostBinding,
	ElementRef,
	ChangeDetectionStrategy,
	ChangeDetectorRef
} from '@angular/core';
import { MatFormFieldControl } from '@angular/material';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { Subject, Subscription, BehaviorSubject } from 'rxjs';
import { NgControl, ControlValueAccessor } from '@angular/forms';
import { TinyMCESettings } from '@codeforfun/admin/models';
import { FocusMonitor } from '@angular/cdk/a11y';
import { EditorManager } from 'tinymce';
import { Utilities } from '@codeforfun/admin/utilities';
import { environment } from '../../../../environments/environment';
import { tap, delay, distinctUntilChanged, debounceTime } from 'rxjs/operators';

declare const tinyMCE: EditorManager;

@Component({
	selector: 'admin-app-tinymce',
	templateUrl: './tinymce.component.html',
	styleUrls: ['./tinymce.component.scss'],
	providers: [{ provide: MatFormFieldControl, useExisting: TinymceComponent }],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class TinymceComponent implements MatFormFieldControl<string>, ControlValueAccessor, AfterViewInit, OnDestroy {
	public static nextId = 0;

	@HostBinding() id = `tinymce-mat-form-field-${TinymceComponent.nextId++}`;

	@Input() delayInit = 0;
	@Input() focusOnInit = false;

	@Input()
	public get required(): boolean {
		return this._required;
	}
	public set required(value: boolean) {
		this._required = coerceBooleanProperty(value);
		this.stateChanges.next();
	}

	@Input()
	get placeholder() {
		return this._placeholder;
	}
	set placeholder(plh: string) {
		this._placeholder = plh;
		this.stateChanges.next();
	}

	@Input()
	get disabled() {
		return this._disabled;
	}
	set disabled(dis: boolean) {
		this._disabled = coerceBooleanProperty(dis);
		this.stateChanges.next();
	}

	public stateChanges = new Subject<void>();
	public shouldLabelFloat = true;
	public controlType = 'tinymce-mat-form-field';
	public focused: boolean;

	public tinyMceSettings: TinyMCESettings;

	private _isInit$ = new BehaviorSubject(false);
	private _isInitWithDelay$ = new BehaviorSubject(false);
	private _required = false;
	private _errorState = false;
	private _disabled = false;
	private _placeholder: string;
	private _subscriptions: Subscription[] = [];

	constructor(
		@Optional() @Self() public ngControl: NgControl,
		private _elementRef: ElementRef<HTMLElement>,
		private _fm: FocusMonitor,
		private _cdr: ChangeDetectorRef
	) {
		if (this.ngControl != null) {
			this.ngControl.valueAccessor = this;
		}
	}

	public ngAfterViewInit() {
		setTimeout(() => {
			this._setupTinyMCE();
			this._setupFormEvent();
			this._setupFocusMonitor();
			this._setupOtherEvent();
		}, this.delayInit);
	}

	public ngOnDestroy() {
		this.stateChanges.complete();
		this._fm.stopMonitoring(this._elementRef.nativeElement);
		Utilities.unsubcribe(this._subscriptions);
	}

	public set value(val: string) {
		this.stateChanges.next();
	}

	public get errorState() {
		return this._errorState;
	}

	public get empty() {
		if (!tinyMCE.activeEditor || !this.ngControl || !this.ngControl.control) {
			return true;
		}

		return this.ngControl.control.value == null || this.ngControl.control.value === '';
	}

	public setDescribedByIds(ids: string[]): void {}

	public onContainerClick(event: MouseEvent): void {}

	public onClick() {
		this._updateFocusState(true);
		this._markAsTouched();
	}

	public onBlur() {
		this._updateFocusState(false);
		this._updateErrorState();
	}

	public onInit() {
		this._isInit$.next(true);

		if (this.focusOnInit && tinyMCE.activeEditor) {
			tinyMCE.activeEditor.focus(false);
			tinyMCE.activeEditor.selection.select(tinyMCE.activeEditor.getBody(), true);
			tinyMCE.activeEditor.selection.collapse(false);
			this._updateFocusState(true);
			this._updateErrorState();
			this._markAsTouched();
		}

		this._elementRef.nativeElement.click();
	}

	public get isInit$() {
		return this._isInit$.asObservable();
	}

	/* ****************************************************
	 * Control value accessor
	 * ****************************************************/

	public writeValue(obj: any): void {}

	public registerOnChange(fn: any): void {}

	public registerOnTouched(fn: any): void {}

	public setDisabledState?(isDisabled: boolean): void {}

	/* ****************************************************
	 * Private
	 * ****************************************************/

	private _setupTinyMCE() {
		tinyMCE['baseURL'] = `${environment.baseUrl}${environment.tinyMceBasePath}`;

		this.tinyMceSettings = {
			skin_url: environment.tinyMceSkinPath,
			autoresize_min_height: 400,
			autoresize_max_height: 400,
			theme: 'modern',
			plugins: `advlist anchor autolink autoresize code codesample colorpicker help hr image imagetools insertdatetime link lists media pagebreak paste preview print searchreplace tabfocus table textcolor textpattern toc visualblocks visualchars wordcount`,
			toolbar: `formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat pagebreak`
		};

		this._cdr.detectChanges();
	}

	private _setupFormEvent() {
		if (this.ngControl && this.ngControl.control) {
			this._subscriptions.push(
				this.ngControl.control.valueChanges
					.pipe(
						distinctUntilChanged(),
						debounceTime(200)
					)
					.subscribe(_ => {
						this._markAsTouched();
						this._updateErrorState();
					})
			);
		}
	}

	private _setupFocusMonitor() {
		this._fm.monitor(this._elementRef.nativeElement, true).subscribe(() => {
			this.focused = false;
			this.stateChanges.next();
		});
	}

	private _setupOtherEvent() {
		this.isInit$
			.pipe(
				delay(200),
				tap(val => {
					this._isInitWithDelay$.next(val);
				})
			)
			.subscribe();
	}

	private _updateErrorState() {
		if (
			this.ngControl &&
			this.ngControl.control &&
			this._isInit$.value &&
			this._isTouched &&
			tinyMCE.activeEditor
		) {
			this._errorState = this.ngControl.control.errors != null;
			this.stateChanges.next();
			this._elementRef.nativeElement.click();
		}
	}

	private _updateFocusState(isFocus: boolean) {
		this.focused = isFocus;
		this.stateChanges.next();
		this._elementRef.nativeElement.click();
	}

	private _markAsTouched() {
		if (this.ngControl) {
			this.ngControl.control.markAsTouched({
				onlySelf: true
			});
		}
	}

	private get _isTouched() {
		return this.ngControl && this.ngControl.control.touched;
	}
}
