import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule, NG_VALUE_ACCESSOR } from '@angular/forms';
import { MatDialogModule, MatProgressSpinnerModule, MatSnackBarModule, MatButtonModule } from '@angular/material';
import { EditorModule } from '@tinymce/tinymce-angular';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { SnackbarMessageComponent } from './snackbar-message/snackbar-message.component';
import { TinymceComponent } from './tinymce/tinymce.component';
import { ErrorMessageDirective } from './error-message/error-message.directive';

const commonComponents = [ConfirmDialogComponent, SnackbarMessageComponent, TinymceComponent, ErrorMessageDirective];

@NgModule({
	declarations: [commonComponents],
	imports: [
		FormsModule,
		ReactiveFormsModule,
		CommonModule,
		MatDialogModule,
		MatSnackBarModule,
		MatProgressSpinnerModule,
		MatButtonModule,
		EditorModule
	],
	exports: [commonComponents]
})
export class AdminAppCommonModule {}
