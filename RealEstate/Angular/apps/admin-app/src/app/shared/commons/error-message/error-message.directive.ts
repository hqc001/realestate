import { Directive, ElementRef, Optional, Self, OnInit, Input } from '@angular/core';
import { NgControl, ControlValueAccessor } from '@angular/forms';
import { filter, distinctUntilChanged } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

@Directive({
	selector: 'mat-error'
})
export class ErrorMessageDirective implements OnInit, ControlValueAccessor {
	@Input() translate = {};

	private _currentError = '';

	constructor(
		@Optional() @Self() private _ngControl: NgControl,
		private _elementRef: ElementRef<HTMLElement>,
		private _translate: TranslateService
	) {
		if (this._ngControl) {
			this._ngControl.valueAccessor = this;
		}
	}

	public ngOnInit() {
		if (this._ngControl && this._ngControl.control) {
			if (this._ngControl.control.status === 'INVALID') {
				this._setErrorMessage();
			}

			this._ngControl.control.statusChanges
				.pipe(
					filter(x => x === 'INVALID'),
					distinctUntilChanged()
				)
				.subscribe(e => {
					this._setErrorMessage();
				});
		}
	}

	public writeValue(obj: any): void {}
	public registerOnChange(fn: any): void {}
	public registerOnTouched(fn: any): void {}
	public setDisabledState?(isDisabled: boolean): void {}

	private _setErrorMessage() {
		const error = Object.keys(this._ngControl.control.errors || {})[0] || '';
		if (error !== this._currentError) {
			this._currentError = error;
			this._translate.get(`errorMessage.${error}`, this.translate).subscribe(val => {
				this._elementRef.nativeElement.textContent = val;
			});
		}
	}
}
