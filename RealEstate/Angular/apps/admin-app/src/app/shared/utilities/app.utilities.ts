import { Subscription } from 'rxjs';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

export class Utilities {
	public static unsubcribe(subs: Subscription[]) {
		subs.forEach(sub => {
			if (!sub.closed) {
				sub.unsubscribe();
			}
		});
	}

	public static buildForm<T>(formBuilder: FormBuilder, data: FormGroupData<T>) {
		return formBuilder.group(data);
	}

	public static getCurrentActivatedRouteChild(activatedRoute: ActivatedRoute): ActivatedRoute {
		if (!activatedRoute.firstChild) {
			return activatedRoute;
		}

		if (activatedRoute.firstChild) {
			return Utilities.getCurrentActivatedRouteChild(activatedRoute.firstChild);
		}
	}

	public static updateValueAndValidity(formGroup: FormGroup) {
		for (const k in formGroup.controls) {
			if (formGroup.controls[k]) {
				const control = formGroup.controls[k];

				if (control instanceof FormGroup) {
					if (control.controls) {
						Utilities.updateValueAndValidity(control);
					}
				} else {
					control.updateValueAndValidity({ emitEvent: false });
					control.markAsTouched({ onlySelf: true });
				}
			}
		}
	}
}
