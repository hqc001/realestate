import { Component, ChangeDetectionStrategy } from '@angular/core';
import { AuthenticationService } from '@codeforfun/admin/services';
import { ILoginForm } from '@codeforfun/admin/models';

@Component({
	selector: 'admin-app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent {
	public loginFormModel: ILoginForm = {
		userName: '',
		password: '',
		remember: false
	};

	constructor(public authService: AuthenticationService) {}

	public login() {
		this.authService.doLogin(this.loginFormModel);
	}
}
