import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { BlogComponent } from './blog/blog.component';
import { ProjectComponent } from './project/project.component';
import { LayoutStyle, IRouteData } from './model/layout.model';
import { ProjectResolver } from './project/services/project.resolver';
import { BlogResolver } from './blog/services/blog.resolver';

const routes: Routes = [
	{
		path: '',
		component: HomeComponent,
		data: {
			layoutStyle: LayoutStyle.HomePage,
			title: 'Căn hộ SG 24h'
		} as IRouteData
	},
	{
		path: 'tin-tuc/:blogUID',
		component: BlogComponent,
		resolve: {
			blog: BlogResolver
		},
		data: {
			layoutStyle: LayoutStyle.Other
		} as IRouteData
	},
	// TODO
	// {
	// 	path: 'du-an',
	// 	component: ProjectListComponent,
	// 	data: {
	// 		headerStyle: LayoutStyle.Other,
	// 		title: 'Các dự án nhà đất, căn hộ chung cư'
	// 	} as IRouteData
	// },
	{
		path: 'du-an/:projectUID',
		component: ProjectComponent,
		resolve: {
			project: ProjectResolver
		},
		data: {
			layoutStyle: LayoutStyle.Other
		} as IRouteData
	},
	{
		path: '**',
		redirectTo: ''
	}
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes, {
			anchorScrolling: 'enabled',
			paramsInheritanceStrategy: 'always'
		})
	],
	exports: [RouterModule]
})
export class AppRoutingModule {}
