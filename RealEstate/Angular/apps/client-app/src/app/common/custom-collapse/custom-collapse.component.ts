import { Component, Input } from '@angular/core';

@Component({
	selector: 'client-app-custom-collapse',
	templateUrl: './custom-collapse.component.html',
	styleUrls: ['./custom-collapse.component.scss']
})
export class CustomCollapseComponent {
	@Input() isCollapse: boolean;
	@Input() header1;
	@Input() header2;
	constructor() {}
}
