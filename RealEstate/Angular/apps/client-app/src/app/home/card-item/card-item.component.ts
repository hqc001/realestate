import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { ProjectStatus, IHomePageProject } from '../../model/home.model';
import { AppFeatureService } from '../../shared/services/app-feature.service';
import { environment } from '../../../environments/environment';

@Component({
	selector: 'client-app-card-item',
	templateUrl: './card-item.component.html',
	styleUrls: ['./card-item.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardItemComponent {
	@Input() project: IHomePageProject;

	public ProjectStatus = ProjectStatus;

	constructor(private _appFtSv: AppFeatureService) {}

	public getAddress(project: IHomePageProject) {
		return this._appFtSv.getAddress(project.district, project.city);
	}

	public getThumbnaiImage(thumbnailName: string) {
		return environment.uploadUrl + thumbnailName;
	}
}
