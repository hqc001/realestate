import { Component } from '@angular/core';
import { ProjectApiService } from '../apis/project.api';
import { IHomePageProject } from '../model/home.model';
import { AppFeatureService } from '../shared/services/app-feature.service';

@Component({
	selector: 'client-app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent {
	public data: IHomePageProject[];

	constructor(private _projectApi: ProjectApiService, public appFtSv: AppFeatureService) {
		this._projectApi.getHomePageProjects().subscribe(data => {
			this.data = data;
		});
	}
}
