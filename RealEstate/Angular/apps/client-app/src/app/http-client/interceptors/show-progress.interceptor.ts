import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEventType } from '@angular/common/http';
import { NgProgress, NgProgressRef } from '@ngx-progressbar/core';
import { tap } from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})
export class ShowProgressInterceptor implements HttpInterceptor {
	private _pendingRequest = 0;
	private _progressRef: NgProgressRef;

	constructor(private _progress: NgProgress) {
		this._progressRef = this._progress.ref();
	}

	public intercept(req: HttpRequest<any>, next: HttpHandler) {
		return next.handle(req).pipe(
			tap(event => {
				switch (true) {
					case event.type === HttpEventType.Sent: {
						this._pendingRequest++;

						if (!this._progressRef.isStarted) {
							this._progressRef.start();
						}

						break;
					}
					case event.type === HttpEventType.Response: {
						// in case multiple request call in same time
						setTimeout(() => {
							this._pendingRequest--;

							if (this._pendingRequest === 0) {
								this._progressRef.complete();
							}
						}, 100);
						break;
					}
				}
			})
		);
	}
}
