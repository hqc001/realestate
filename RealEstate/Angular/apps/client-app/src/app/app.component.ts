import { ViewportScroller } from '@angular/common';
import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter, map } from 'rxjs/operators';
import { LayoutService } from './layout/services/layout.service';
import { IRouteData } from './model/layout.model';
import { AppFeatureService } from './shared/services/app-feature.service';

@Component({
	selector: 'client-app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent {
	constructor(
		private _router: Router,
		private _activatedRoute: ActivatedRoute,
		private _viewportScroller: ViewportScroller,
		private _layoutService: LayoutService,
		private _titleService: Title,
		private _appFtSv: AppFeatureService
	) {
		this._registerRouteEvent();
		this._appFtSv.getCitiesAndDistricts();
	}

	private _registerRouteEvent() {
		this._router.events
			.pipe(
				filter(event => event instanceof NavigationEnd),
				map(() => this._activatedRoute.firstChild),
				map(activatedRoute => {
					while (activatedRoute.firstChild != null) {
						activatedRoute = activatedRoute.firstChild;
					}
					return activatedRoute;
				})
			)
			.subscribe((activatedRoute: ActivatedRoute) => {
				const routeData = activatedRoute.snapshot.data as IRouteData;
				this._layoutService.setLayoutStyle(routeData.layoutStyle);
				this._titleService.setTitle(routeData.title || 'Căn hộ SG 24h');
				this._viewportScroller.scrollToPosition([0, 0]);
			});
	}
}
