import { Component, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { HttpClient } from '@angular/common/http';

import { AdvancedLayout, GridLayout, Image, PlainGalleryConfig, PlainGalleryStrategy } from 'angular-modal-gallery';
import { IProject } from '../model/project.model';
import { ICustomerRequestForm } from '../model/customer-request.model';
import { IBlog } from '../model/blog.model';
import { ProjectDataService } from './services/project-data.service';
import { environment } from '../../environments/environment';
import { ProjectStatus, ProjectTypes } from '../model/home.model';

interface IProjectDataSource {
	project: IProject;
}

@Component({
	selector: 'client-app-project',
	templateUrl: './project.component.html',
	styleUrls: ['./project.component.scss']
})
export class ProjectComponent {
	@ViewChild('modalTemplate') modalTemplate: TemplateRef<any>;
	public ProjectStatus = ProjectStatus;
	public ProjectTypes = ProjectTypes;
	public project: IProject;
	public customerRequestForm: ICustomerRequestForm = {
		firstName: '',
		email: '',
		lastName: '',
		message: '',
		phone: ''
	};

	isCollapsed = true;
	title = '';
	tempHeader = {
		summary: 'Thông tin chi tiết',
		internalAdvance: 'Tiện ích nội khu',
		externalAdvance: 'Tiện ích ngoại khu',
		design: 'Thiết kế',
		addendum: 'Phụ lục',
		location: 'Vị trí dự án',
		preference: 'Ưu Đãi'
	};

	blogs: IBlog[];

	// google maps zoom level
	zoom = 16;
	// initial center position for the map
	lat = 10.729501;
	lng = 106.729506;

	markers: Marker[] = [
		{
			lat: 10.729501,
			lng: 106.729506,
			draggable: false
		}
	];

	images: Image[] = [];

	customPlainGalleryRowConfig: PlainGalleryConfig = {
		strategy: PlainGalleryStrategy.CUSTOM,
		layout: new AdvancedLayout(-1, true)
	};

	plainGalleryGrid: PlainGalleryConfig = {
		strategy: PlainGalleryStrategy.GRID,
		layout: new GridLayout({ width: '200px', height: '200px' }, { length: 3, wrap: true })
	};

	modalRef: BsModalRef;
	blogSrc: string;

	constructor(
		private modalService: BsModalService,
		private _httpClient: HttpClient,
		private route: ActivatedRoute,
		private projectService: ProjectDataService
	) {
		projectService.projects$.subscribe(data => {
			console.log(data);
			this.project = data;
			if (this.project.images) {
				for (let i = 0; i < this.project.images.length; i++) {
					this.images.push(
						new Image(i, {
							// modal
							img: environment.uploadUrl + this.project.images[i].name
						})
					);
				}
			}
		});

		// projectService.getProjectTop3Blog(this.project.id).subscribe(x => {
		// 	this.blogs = x;
		// });
	}

	public openImageModalRow(image: Image) {
		const index: number = this.getCurrentIndexCustomLayout(image, this.images);
		this.customPlainGalleryRowConfig = Object.assign({}, this.customPlainGalleryRowConfig, {
			layout: new AdvancedLayout(index, true)
		});
	}

	private getCurrentIndexCustomLayout(image: Image, images: Image[]): number {
		return image ? images.indexOf(image) : -1;
	}

	public openModal() {
		this.modalRef = this.modalService.show(this.modalTemplate);
	}

	public sendRequest() {
		this._httpClient.post(environment.baseUrl + '/api/customerrequest', this.customerRequestForm).subscribe(rs => {
			this.modalRef.hide();
		});
	}
}

// just an interface for type safety.
class Marker {
	lat: number;
	lng: number;
	label?: string;
	draggable: boolean;
}
