import { Component, Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'client-app-project-sale-information',
	templateUrl: './project-sale-information.component.html',
	styleUrls: ['./project-sale-information.component.scss']
})
export class ProjectSaleInformationComponent {
	@Output() openModalEvent = new EventEmitter();

	constructor() {}

	public openModal() {
		this.openModalEvent.emit(null);
	}
}
