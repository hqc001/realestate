import { Component, OnInit, Input } from '@angular/core';
import { Image, PlainGalleryConfig, AdvancedLayout, GridLayout, PlainGalleryStrategy } from 'angular-modal-gallery';
import { IProjectSummary, IProject } from '../../model/project.model';
import { ProjectDataService } from '../services/project-data.service';
import { AppFeatureService } from '../../shared/services/app-feature.service';

@Component({
	selector: 'client-app-project-header',
	templateUrl: './project-header.component.html',
	styleUrls: ['./project-header.component.scss']
})
export class ProjectHeaderComponent implements OnInit {
	@Input() images: Image[];
	district: string;
	project: IProject;
	customPlainGalleryRowConfig: PlainGalleryConfig = {
		strategy: PlainGalleryStrategy.CUSTOM,
		layout: new AdvancedLayout(-1, true)
	};
	plainGalleryGrid: PlainGalleryConfig = {
		strategy: PlainGalleryStrategy.GRID,
		layout: new GridLayout({ width: '200px', height: '200px' }, { length: 3, wrap: true })
	};

	constructor(public projectService: ProjectDataService, public appFtSv: AppFeatureService) {
		projectService.projects$.subscribe(data => {
			this.project = data;
			if (this.project) {
				this.district = appFtSv.getDistrict(this.project.district, this.project.city);
			}
		});
	}

	ngOnInit() {}

	openImageModalRow(image: Image) {
		const index: number = this.getCurrentIndexCustomLayout(image, this.images);
		this.customPlainGalleryRowConfig = Object.assign({}, this.customPlainGalleryRowConfig, {
			layout: new AdvancedLayout(index, true)
		});
	}

	private getCurrentIndexCustomLayout(image: Image, images: Image[]): number {
		return image ? images.indexOf(image) : -1;
	}
}
