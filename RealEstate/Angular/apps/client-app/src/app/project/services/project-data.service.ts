import { Injectable } from '@angular/core';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { BlogApiService } from '../../apis/blog.api';
import { ProjectApiService } from '../../apis/project.api';
import { IBlog } from '../../model/blog.model';
import { IProject } from '../../model/project.model';

@Injectable({
	providedIn: 'root'
})
export class ProjectDataService {
	constructor(private _projectApi: ProjectApiService) {}

	private _initData: IProject = {
		id: 0,
		invesloperId: null,
		appendix: null,
		area: null,
		block: null,
		city: null,
		code: null,
		densityOfBuilding: null,
		design: null,
		district: null,
		floor: null,
		handoverDate: null,
		insideUtilities: null,
		lat: 0,
		lng: 0,
		name: null,
		outsideUtilities: null,
		penthouse: null,
		preferential: null,
		price: null,
		startDate: null,
		statusId: null,
		type: null,
		uid: null,
		unit: null,
		displayImageId: null,
		isGoodPrice: false,
		images: null,
		introduction: null,
		address: null,
		locationDescription: null,
		invesloper: null
	};

	private _projects$ = new BehaviorSubject<IProject>(this._initData);
	public projects$ = this._projects$.asObservable();
	/**
	 * Fake api get project by UID
	 * @param uid: uid of project
	 */
	public getProjectByUID(uid: string) {
		this._projectApi.getProject(uid).subscribe(data => this._projects$.next(data));
	}

	// public getProjectTop3Blog(projectId: number): Observable<IBlog[]> {
	// 	return this._blogApi.getBlogJsonData().pipe(
	// 		switchMap((data: any) => {
	// 			if (data && data.blogs) {
	// 				return of(data.blogs);
	// 			}

	// 			return of(null);
	// 		})
	// 	);
	// }
}
