import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router, Resolve } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { ProjectDataService } from './project-data.service';
import { switchMap } from 'rxjs/operators';
import { IProject } from '../../model/project.model';

@Injectable({
	providedIn: 'root'
})
export class ProjectResolver implements Resolve<IProject> {
	constructor(private _projectApi: ProjectDataService, private _router: Router) {}

	public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
		if (route.params['projectUID']) {
			this._projectApi.getProjectByUID(route.params['projectUID']);
		}

		return true;
	}
}
