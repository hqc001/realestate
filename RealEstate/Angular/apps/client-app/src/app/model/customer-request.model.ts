export interface ICustomerRequestForm {
	firstName: string;
	lastName: string;
	email: string;
	phone: string;
	message: string;
}
