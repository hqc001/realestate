export enum ProjectStatus {
	Closed = 1,
	Opened = 2,
	ComingSoon = 3
}

export enum ProjectTypes {
	Housing = 1
}

export interface IHomePageProject {
	id: number;
	uid: string;
	displayImage: any;
	isGoodPrice: boolean;
	statusId: number;
	name: string;
	city: string;
	district: string;
	price: string;
	area: string;
}
