export interface IBlog {
	projectId: string;
	blogId: number;
	blogUID: string;
	title: string;
	author: string;
	createDate: Date;
	summary: string;
	detail: string;
	thumbImgname: string;
}
