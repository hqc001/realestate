export enum LayoutStyle {
	HomePage,
	Other,
	None
}

export interface IRouteData {
	layoutStyle: LayoutStyle;
	title?: string;
}
