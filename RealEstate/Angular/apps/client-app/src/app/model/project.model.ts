export interface IProject {
	id: number;
	uid: string;
	name: string;
	area: string;
	block: number;
	city: string;
	code: string;
	densityOfBuilding: number;
	district: string;
	floor: string;
	handoverDate: Date;
	penthouse: string;
	price: string;
	startDate: Date;
	statusId: number;
	type: string;
	unit: number;
	lat: number;
	lng: number;
	invesloperId: number;
	insideUtilities: string;
	outsideUtilities: string;
	design: string;
	appendix: string;
	preferential: string;
	displayImageId?: number;
	isGoodPrice: boolean;
	images: IImage[];
	introduction: string;
	locationDescription: string;
	address: string;
	invesloper: IInvesloper;
}

export interface IInvesloper {
	description: string;
	url: string;
	name: string;
}

export interface IImage {
	name: string;
	thumbnailName: string;
}

export interface IProjectSummary {
	name: string;
	district: string;
	street: string;
	status: string;
	invesloper: string;
	invesloperURL: string;
	invesloperIntroduce: string;
	type: string;
	blocks: string;
	floors: string;
	unit: number;
	price: string;
	introduce: string;
	location: string;
	internalAdvance: string;
	externalAdvance: string;
	design: string;
	addendum: string;
	preference: string;
	lat: number;
	lng: number;
}

export interface IProjectDetail {
	startDate: Date;
	handoverDate: Date;
	penthouse: number;
	densityOfBuilding: number;
	area: string;
}

export interface SelectOption<TValue> {
	value: TValue;
	description: string;
}

export interface ICitiesAndDistricts {
	[key: string]: {
		name: string;
		cities: {
			[key: string]: string;
		};
	};
}
