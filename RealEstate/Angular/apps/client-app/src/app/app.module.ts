/* *******************************************************
 * Angula Core
 * *******************************************************/
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

/* *******************************************************
 * 3rd Parties
 * *******************************************************/
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ModalGalleryModule } from 'angular-modal-gallery';
import { AgmCoreModule } from '@agm/core';
import { NgProgressModule } from '@ngx-progressbar/core';
import { AppHttpClientModule } from '@codeforfun/http-client';
import 'hammerjs';
import 'mousetrap';

/* *******************************************************
 * App components
 * *******************************************************/
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { BlogComponent } from './blog/blog.component';
import { ProjectComponent } from './project/project.component';
import { CardItemComponent } from './home/card-item/card-item.component';
import { ProjectSaleInformationComponent } from './project/project-sale-information/project-sale-information.component';
import { ProjectHeaderComponent } from './project/project-header/project-header.component';
import { CustomCollapseComponent } from './common/custom-collapse/custom-collapse.component';
import { ProjectListComponent } from './project-list/project-list.component';
import { ProjectListItemComponent } from './project-list/project-list-item/project-list-item.component';
import { MainHeaderComponent } from './layout/main-header/main-header.component';
import { SubHeaderComponent } from './layout/sub-header/sub-header.component';
import { FooterComponent } from './layout/footer/footer.component';
import { LayoutComponent } from './layout/layout.component';

/* *******************************************************
 * App services / pipes / others
 * *******************************************************/
import { SafeHtmlPipe } from './pipes/safe-html.pipe';
import { HeaderNavbarComponent } from './layout/header-navbar/header-navbar.component';
import { ShowProgressInterceptor } from './http-client/interceptors/show-progress.interceptor';
import { environment } from '../environments/environment';

@NgModule({
	declarations: [
		AppComponent,
		HomeComponent,
		BlogComponent,
		ProjectComponent,
		CardItemComponent,
		ProjectSaleInformationComponent,
		ProjectHeaderComponent,
		CustomCollapseComponent,
		ProjectListComponent,
		ProjectListItemComponent,
		SafeHtmlPipe,
		MainHeaderComponent,
		SubHeaderComponent,
		FooterComponent,
		LayoutComponent,
		HeaderNavbarComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		BsDropdownModule.forRoot(),
		CollapseModule.forRoot(),
		AppHttpClientModule.forRoot({
			baseUrl: environment.baseUrl
		}),
		AgmCoreModule.forRoot({
			apiKey: 'AIzaSyAtQrX8kwc5oU0rVKCwQIjphM0X8Fb2q4o'
		}),
		ModalGalleryModule.forRoot(),
		ModalModule.forRoot(),
		NgProgressModule.forRoot(),
		FormsModule
	],
	providers: [
		{
			provide: HTTP_INTERCEPTORS,
			useClass: ShowProgressInterceptor,
			multi: true
		}
	],
	bootstrap: [AppComponent]
})
export class AppModule {}
