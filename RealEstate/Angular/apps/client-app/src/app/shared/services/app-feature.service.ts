import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { ProjectApiService } from '../../apis/project.api';
import { SelectOption, ICitiesAndDistricts } from '../../model/project.model';

@Injectable({
	providedIn: 'root'
})
export class AppFeatureService {
	private _cities$ = new BehaviorSubject<SelectOption<string>[]>(null);
	private _districts$ = new BehaviorSubject<SelectOption<string>[]>(null);
	private _citiesAndDistricts$ = new BehaviorSubject<ICitiesAndDistricts>(null);

	public cities$ = this._cities$.asObservable();
	public districts$ = this._districts$.asObservable();

	constructor(private _projectApi: ProjectApiService) {}

	public getCitiesAndDistricts() {
		this._projectApi.getCitiesAndDistricts().subscribe(data => {
			this._citiesAndDistricts$.next(data);
			const cities: SelectOption<string>[] = [];
			for (const key in data) {
				if (data.hasOwnProperty(key)) {
					cities.push({
						value: key,
						description: data[key].name
					});
				}
			}

			this._cities$.next(cities);
		});
	}

	public getDistrictsByCityKey(cityKey: string) {
		const data = this._citiesAndDistricts$.value || {};
		const districts: SelectOption<string>[] = [];

		const distrs = data[cityKey] ? data[cityKey].cities || {} : {};
		for (const distrKey in distrs) {
			if (distrs.hasOwnProperty(distrKey)) {
				districts.push({
					value: distrKey,
					description: distrs[distrKey]
				});
			}
		}

		this._districts$.next(districts);
	}

	public getAddress(districtKey: string, cityKey: string) {
		const data = this._citiesAndDistricts$.value || {};
		const distrs = data[cityKey] ? data[cityKey].cities || {} : {};

		const city = data[cityKey] || {};
		const district = distrs[districtKey] || '';

		return `${district} ${city['name']}`;
	}

	public getDistrict(districtKey: string, cityKey: string) {
		const data = this._citiesAndDistricts$.value || {};
		const distrs = data[cityKey] ? data[cityKey].cities || {} : {};

		const city = data[cityKey] || {};
		const district = distrs[districtKey] || '';

		return `${district}`;
	}

	public getCity(cityKey: string) {
		const data = this._citiesAndDistricts$.value || {};

		const city = data[cityKey] || {};

		return `${city['name']}`;
	}

	public resetDistricts() {
		this._districts$.next(null);
	}
}
