import { Component } from '@angular/core';
import { LayoutService } from './services/layout.service';
import { LayoutStyle } from '../model/layout.model';

@Component({
	selector: 'client-app-layout',
	templateUrl: './layout.component.html',
	styles: [
		`
			.main-content {
				min-height: 750px;
			}
		`
	]
})
export class LayoutComponent {
	public LayoutStyle = LayoutStyle;

	constructor(public layoutService: LayoutService) {}
}
