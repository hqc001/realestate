import { Component } from '@angular/core';

@Component({
	selector: 'client-app-sub-header',
	templateUrl: './sub-header.component.html',
	styleUrls: ['./sub-header.component.scss']
})
export class SubHeaderComponent {}
