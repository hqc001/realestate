import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { LayoutStyle } from '../../model/layout.model';

@Injectable({
	providedIn: 'root'
})
export class LayoutService {
	private _layoutStyle = new BehaviorSubject<LayoutStyle>(LayoutStyle.None);

	public layoutStyle$ = this._layoutStyle.asObservable();

	public setLayoutStyle(layoutStyle: LayoutStyle) {
		this._layoutStyle.next(layoutStyle);
	}
}
