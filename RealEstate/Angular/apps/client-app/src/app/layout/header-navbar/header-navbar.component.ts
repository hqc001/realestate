import { Component, OnDestroy } from '@angular/core';
import { LayoutService } from '../services/layout.service';
import { LayoutStyle } from '../../model/layout.model';
import { Subscription } from 'rxjs';

@Component({
	selector: 'client-app-header-navbar',
	templateUrl: './header-navbar.component.html',
	styleUrls: ['./header-navbar.component.scss']
})
export class HeaderNavbarComponent implements OnDestroy {
	public HeaderStyle = LayoutStyle;
	public logoClass: any;
	public navClass: any;

	private _subscriptions: Subscription[] = [];

	constructor(public layoutService: LayoutService) {
		this._subscriptions.push(
			layoutService.layoutStyle$.subscribe(headerStyle => {
				switch (headerStyle) {
					case LayoutStyle.HomePage: {
						this.logoClass = {
							'text-white': true
						};

						this.navClass = {};
						break;
					}
					case LayoutStyle.Other: {
						this.logoClass = {
							'text-danger': true
						};

						this.navClass = {
							'sub-header': true,
							'border-bottom': true
						};
						break;
					}
				}
			})
		);
	}

	public ngOnDestroy(): void {
		this._subscriptions.forEach(sub => {
			if (!sub.closed) {
				sub.unsubscribe();
			}
		});
	}
}
