import { Injectable } from '@angular/core';
import { HttpAbstractService, GET, Query } from '@codeforfun/http-client';
import { Observable } from 'rxjs';
import { IProject, ICitiesAndDistricts } from '../model/project.model';
import { IHomePageProject } from '../model/home.model';

@Injectable({
	providedIn: 'root'
})
export class ProjectApiService extends HttpAbstractService {
	// @GET('assets/data/project.data.json')
	// public getProjectJsonData(): Observable<IProject> {
	// 	return null;
	// }

	@GET('api/project')
	public getHomePageProjects(): Observable<IHomePageProject[]> {
		return null;
	}

	@GET('api/project/detail')
	public getProject(@Query('uid') uid: string): Observable<IProject> {
		return null;
	}

	@GET('assets/data/cities.json')
	public getCitiesAndDistricts(): Observable<ICitiesAndDistricts> {
		return null;
	}
}
