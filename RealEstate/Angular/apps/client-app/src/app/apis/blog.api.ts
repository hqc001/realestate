import { Injectable } from '@angular/core';
import { HttpAbstractService, GET } from '@codeforfun/http-client';
import { Observable } from 'rxjs';
import { IBlog } from '../model/blog.model';

@Injectable({
	providedIn: 'root'
})
export class BlogApiService extends HttpAbstractService {
	@GET('assets/data/blog.data.json')
	public getBlogJsonData(): Observable<IBlog[]> {
		return null;
	}
}
