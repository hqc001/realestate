import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router, Resolve } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { BlogDataService } from './blog-data.service';
import { IBlog } from '../../model/blog.model';

@Injectable({
	providedIn: 'root'
})
export class BlogResolver implements Resolve<IBlog> {
	constructor(private _blogData: BlogDataService, private _router: Router) {}

	public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IBlog> {
		return this._blogData.getBlogByUID(route.params['blogUID']).pipe(
			switchMap((data: any) => {
				if (data) {
					return of(data);
				}

				this._router.navigate(['/']);
				return EMPTY;
			})
		);
	}
}
