import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { IBlog } from '../../model/blog.model';
import { BlogApiService } from '../../apis/blog.api';

@Injectable({
	providedIn: 'root'
})
export class BlogDataService {
	constructor(private _blogApi: BlogApiService) {}

	/**
	 * Fake api get project by UID
	 * @param uid: uid of project
	 */
	public getBlogByUID(blogUid: string): Observable<IBlog> {
		return this._blogApi.getBlogJsonData().pipe(
			switchMap((data: any) => {
				if (data && data.blogs && data.blogs.some(x => x.blogUID === blogUid)) {
					return of(data.blogs.filter(x => x.blogUID === blogUid)[0]);
				}

				return of(null);
			})
		);
	}
}
