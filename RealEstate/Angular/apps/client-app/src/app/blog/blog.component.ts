import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ActivatedRoute } from '@angular/router';
import { take, map } from 'rxjs/operators';
import { IBlog } from '../model/blog.model';

@Component({
	selector: 'client-app-blog',
	templateUrl: './blog.component.html',
	styleUrls: ['./blog.component.scss']
})
export class BlogComponent {
	public blogSrc: string;
	public blog: IBlog;

	constructor(private _httpClient: HttpClient, private route: ActivatedRoute) {
		route.data
			.pipe(
				map(data => data),
				take(1)
			)
			.subscribe(data => {
				this.blog = data.blog as IBlog;

				console.log(this.blog.title);
			});

		// this._httpClient.get('assets/data/Sunshine-City-Q7.html', {
		// 	observe: 'body',
		// 	responseType: 'text'
		// }).subscribe((data: string) => {
		// 	const _data = data.replace(new RegExp(`Sunshine-City-Q7_files`, 'g'), 'assets/data/Sunshine-City-Q7_files');
		// 	this.blogSrc = _data;
		// });
	}
}
