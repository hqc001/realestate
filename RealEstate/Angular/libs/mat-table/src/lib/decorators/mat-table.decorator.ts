import { setData } from '../models/mat-table.model.internal';
import { TableDataSource } from '../datasource/table-datasource';

/** Update data source when call load function */
export function UpdateDataSource(): MethodDecorator {
	return function(
		target: TableDataSource<any>,
		propertyKey: string | symbol,
		descriptor: TypedPropertyDescriptor<any>
	) {
		const loadFunction = descriptor.value as Function;

		descriptor.value = function(...args: any[]) {
			const setDataFunc = target[setData] as Function;
			setDataFunc.call(this, loadFunction.call(this, args));
		};

		return descriptor;
	};
}
