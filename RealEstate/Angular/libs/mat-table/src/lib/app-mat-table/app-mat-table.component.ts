import {
	AfterContentInit,
	AfterViewInit,
	Component,
	ContentChildren,
	Input,
	OnInit,
	QueryList,
	ViewChild,
	ViewChildren,
	ContentChild
} from '@angular/core';
import { MatColumnDef, MatTable, MatPaginator, MatSort, Sort, MatSortHeader } from '@angular/material';
import { TableDataSource } from '../datasource/table-datasource';
import { IColumnSettings, TableSettings, ITableParameter } from '../models/mat-table.model';
import { tap } from 'rxjs/operators';
import { merge } from 'rxjs';
import { resetPagin } from '../models/mat-table.model.internal';

@Component({
	selector: 'app-mat-table',
	templateUrl: './app-mat-table.component.html',
	styleUrls: ['./app-mat-table.component.scss']
})
export class AppMatTableComponent implements OnInit, AfterContentInit, AfterViewInit {
	/* **************************************************************************
	 * More info: https://blog.angular-university.io/angular-material-data-table/
	 * **************************************************************************/

	@Input() settings: TableSettings<any>;
	@Input() dataSource: TableDataSource<any>;

	@ViewChild(MatTable) matTable: MatTable<any>;
	@ViewChildren(MatColumnDef) matColumnDefs: QueryList<MatColumnDef>;
	@ContentChild(MatSort) sort: MatSort;
	@ContentChildren(MatColumnDef) customeMatColumnDefs: QueryList<MatColumnDef>;

	@ViewChild(MatPaginator) paginator: MatPaginator;

	public _displayedColumns = [];
	public _columnSettings = [];

	private get _customMatColumnDefs() {
		return this.customeMatColumnDefs ? this.customeMatColumnDefs.toArray() : [];
	}

	constructor() {}

	public ngOnInit(): void {
		this._validateSetting();
	}

	public ngAfterContentInit() {
		this._setupDisplayedColumns();
	}

	public ngAfterViewInit(): void {
		this._addCustomColumnDefs();
		this._setupPaginEvent();
		this._setupSortEvent();
		this._setupLoadEvent();
		this._initialLoad();
	}

	/* ************************************************************
	 * Private
	 * ************************************************************/

	private _validateSetting() {
		const _isHaveCustomColumnDef = () => {
			return this._customMatColumnDefs.length > 0;
		};

		const _isSettingHaveDefinedCustomColumnDefs = () => {
			return this._customMatColumnDefs.every(colDef => {
				const colSetting = this.settings.columns.find(cs => cs.columnDef === colDef.name);
				return colSetting != null && colSetting.isCustomColDef === true;
			});
		};

		const _isSettingHaveCellDataFunction = () => {
			return this.settings.columns.every(colSetting => {
				return colSetting.isCustomColDef || colSetting.cellData != null;
			});
		};

		if (!_isSettingHaveCellDataFunction()) {
			throw new Error('Missing cell data function.');
		}

		if (_isHaveCustomColumnDef() && !_isSettingHaveDefinedCustomColumnDefs()) {
			throw new Error('Missing setting for custom column template.');
		}
	}

	private _addCustomColumnDefs() {
		if (!this.matTable) {
			throw new Error(`MatTable have not rendered yet.`);
		}

		this._customMatColumnDefs.forEach(col => {
			this.matTable.addColumnDef(col);
			this._displayedColumns.push(col.name);
		});

		if (this.settings.isHaveActionColumn) {
			this._displayedColumns.push(this.settings.actionColumnDef);
		}
	}

	private _setupDisplayedColumns() {
		const _excludeCustomTemplate = (columnSetting: IColumnSettings<any>) => {
			return this._customMatColumnDefs.findIndex(col => col.name === columnSetting.columnDef) === -1;
		};

		this._columnSettings = this.settings.columns.filter(_excludeCustomTemplate);
		this._displayedColumns = this._columnSettings.map(col => col.columnDef);
	}

	private _setupPaginEvent() {
		this.paginator.page
			.pipe(
				tap(page => {
					if (this.dataSource.params.pageSize !== page.pageSize) {
						this.dataSource.params.pageIndex = this.paginator.pageIndex = 0;
						this.dataSource.params.pageSize = page.pageSize;
					} else {
						this.dataSource.params.pageIndex = page.pageIndex;
						this.dataSource.params.pageSize = page.pageSize;
					}
				})
			)
			.subscribe();

		this.dataSource[resetPagin].subscribe(e => {
			this.paginator.pageIndex = 0;
		});
	}

	private _setupSortEvent() {
		if (this.sort) {
			this.sort.sortChange.subscribe((sort: Sort) => {
				this.dataSource.params.sortBy = sort.active;
				this.dataSource.params.sortDirection = sort.direction;
				this.dataSource.params.pageIndex = this.paginator.pageIndex = 0;
			});
		}
	}

	private _initialLoad() {
		setTimeout(() => {
			if (this.settings.initialLoad) {
				this.dataSource.load();
			}
		}, 0);
	}

	private _setupLoadEvent() {
		merge(this.paginator.page, this.sort.sortChange)
			.pipe(tap(() => this.dataSource.load()))
			.subscribe();
	}
}
