import { CollectionViewer } from '@angular/cdk/collections';
import { DataSource } from '@angular/cdk/table';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { delay, take } from 'rxjs/operators';
import { ITableParameter, ITableDataSource } from '../models/mat-table.model';
import { setData, resetPagin } from '../models/mat-table.model.internal';

export abstract class TableDataSource<TDataType> implements DataSource<TDataType> {
	private _data$ = new BehaviorSubject<TDataType[]>([]);
	private _loading$ = new BehaviorSubject(false);
	private _isEmpty$ = new BehaviorSubject(true);
	private _dataLength$ = new BehaviorSubject(0);
	private [resetPagin] = new Subject();

	public loading$ = this._loading$.asObservable();
	public isEmpty$ = this._isEmpty$.asObservable();
	public data$ = this._data$.asObservable();
	public dataLength$ = this._dataLength$.asObservable();
	public totalRecord = 0;

	public params: ITableParameter = {
		sortBy: '',
		sortDirection: '',
		pageIndex: 0,
		pageSize: 10,
		filter: ''
	};

	public connect(_: CollectionViewer): Observable<TDataType[]> {
		return this._data$.asObservable();
	}

	public disconnect(_: CollectionViewer): void {
		this._data$.complete();
		this._loading$.complete();
	}

	private [setData](dataObs: Observable<ITableDataSource<TDataType>>) {
		this._loading$.next(true);
		this._isEmpty$.next(this._data$.value.length === 0);

		dataObs
			.pipe(
				take(1),
				delay(200)
			)
			.subscribe(dataSource => {
				this._data$.next(dataSource.data);
				this._loading$.next(false);
				this._isEmpty$.next(dataSource.data.length === 0);
				this.totalRecord = dataSource.totalRecord;
			});
	}

	private _resetParam() {
		this.params = { ...this.params, pageIndex: 0 };
		this[resetPagin].next();
	}

	/** Reload data and return to first page */
	public loadFirstPage() {
		this._resetParam();
		this.load();
	}

	public abstract load(): Observable<ITableDataSource<TDataType>>;
}
