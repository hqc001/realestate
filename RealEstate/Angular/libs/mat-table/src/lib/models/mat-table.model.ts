export class TableSettings<TDataType> {
	public columns: IColumnSettings<TDataType>[];
	public actionButtons?: ITableActionButton<TDataType>[];
	/** Auto call load function on init */
	public initialLoad = true;
	public actionColumnDef = 'action';

	public get isHaveActionColumn() {
		return this.actionButtons && this.actionButtons.length > 0;
	}

	constructor(params: Partial<TableSettings<TDataType>>) {
		Object.assign(this, params);
	}
}

export interface IColumnSettings<TDataType> {
	columnDef: string;
	header: string;
	cellData?: (data: TDataType) => any;
	isCustomColDef?: boolean;
}

export interface ITableParameter {
	sortBy: string;
	sortDirection: string;
	pageIndex: number;
	pageSize: number;
	filter: string;
}

export interface ITableActionButton<TDataType> {
	name: string;
	iconCss: string;
	color: 'primary' | 'warn' | 'accent';
	action: (data: TDataType) => void;
}

export interface ITableDataSource<T> {
	totalRecord: number;
	data: T[];
}
