import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
	MatPaginatorModule,
	MatSortModule,
	MatProgressSpinnerModule,
	MatTableModule,
	MatButtonModule
} from '@angular/material';
import { AppMatTableComponent } from './app-mat-table/app-mat-table.component';

const matTableModules = [
	MatTableModule,
	MatPaginatorModule,
	MatSortModule,
	MatProgressSpinnerModule,
	MatProgressSpinnerModule,
	MatPaginatorModule,
	MatSortModule,
	MatButtonModule
];

@NgModule({
	imports: [CommonModule, ...matTableModules],
	declarations: [AppMatTableComponent],
	exports: [AppMatTableComponent, ...matTableModules]
})
export class AppMatTableModule {}
