export * from './lib/mat-table.module';
export * from './lib/models/mat-table.model';
export * from './lib/datasource/table-datasource';
export * from './lib/decorators/mat-table.decorator';
