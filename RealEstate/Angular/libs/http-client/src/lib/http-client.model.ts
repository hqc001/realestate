import { HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

export enum ResponeTypes {
	Json = 'json',
	Blob = 'blob',
	Text = 'text'
}

export interface IApiSettings {
	/** Respone type of api, default = json */
	responeType?: ResponeTypes;
	/** Use error handler for this api */
	handleError?: boolean;
	/** Map respone data to other*/
	dataMapper?: (data: any) => any;
}

export class AppHttpClientSetting {
	baseUrl: string;
	/** Default header that apply to all request */
	defaultHeader?: HttpHeaders;
}

export abstract class HttpErrorHandler {
	public abstract handleError(error: HttpErrorResponse): Observable<never>;
}
