import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { PendingRequest } from './http-client.model.internal';

@Injectable()
export class HttpClientStateService {
	private _isLoading$ = new BehaviorSubject(false);
	private [PendingRequest] = 0;

	public get isLoading$() {
		return this._isLoading$.asObservable();
	}

	public get isLoading() {
		return this._isLoading$.value;
	}

	public set isLoading(isLoading: boolean) {
		this._isLoading$.next(isLoading);
	}

	public resetState() {
		this[PendingRequest] = 0;
		this.isLoading = false;
	}
}
