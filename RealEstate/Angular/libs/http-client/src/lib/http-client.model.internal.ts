export const PathMetadataKey = Symbol('Path');
export const QueryMetadataKey = Symbol('Query');
export const BodyMetadataKey = Symbol('Body');
export const PendingRequest = Symbol('PendingRequest');

export interface ParamObject {
	/** key of Query or Path */
	key: string;
	parameterIndex: number;
}
