import { NgModule, Provider, Optional, SkipSelf } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ModuleWithProviders } from '@angular/compiler/src/core';
import { AppHttpClientSetting, HttpErrorHandler } from './http-client.model';
import { HttpClientStateService } from './http-client-state.service';

@NgModule({
	imports: [HttpClientModule],
	exports: [HttpClientModule]
})
export class AppHttpClientModule {
	constructor(@Optional() @SkipSelf() appHttpClientModule: AppHttpClientModule) {
		if (appHttpClientModule) {
			throw new Error('AppHttpClientModule is already loaded. Import it in the AppModule only');
		}
	}

	static forRoot(setting: AppHttpClientSetting): ModuleWithProviders {
		return {
			ngModule: AppHttpClientModule,
			providers: [
				{
					provide: AppHttpClientSetting,
					useValue: setting
				},
				{
					provide: HttpErrorHandler,
					// Dont know why but need to set to null to be able to use instance from parent module.
					// Parent module also need to provide HttpErrorHandler with existing implemented class
					useExisting: null
				},
				HttpClientStateService
			] as Provider[]
		};
	}
}
