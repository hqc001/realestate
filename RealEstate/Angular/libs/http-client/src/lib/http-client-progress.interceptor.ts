import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEventType } from '@angular/common/http';
import { tap, filter, delay } from 'rxjs/operators';
import { HttpClientStateService } from './http-client-state.service';
import { PendingRequest } from './http-client.model.internal';

@Injectable()
export class HttpClientProgressInterceptor implements HttpInterceptor {
	constructor(private _httpStateService: HttpClientStateService) {}

	public intercept(req: HttpRequest<any>, next: HttpHandler) {
		return next.handle(req).pipe(
			tap(event => {
				switch (true) {
					case event.type === HttpEventType.Sent: {
						this._httpStateService[PendingRequest]++;
						this._httpStateService.isLoading = true;
						break;
					}
					case event.type === HttpEventType.Response: {
						// in case multiple request call in same time
						setTimeout(() => {
							this._httpStateService[PendingRequest]--;
							this._httpStateService.isLoading = this._httpStateService[PendingRequest] > 0;
						}, 100);
						break;
					}
				}
			})
		);
	}
}
