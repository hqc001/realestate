// https://github.com/netmedia/angular-architecture-patterns

import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable, Optional } from '@angular/core';
import * as HttpStatus from 'http-status-codes';
import 'reflect-metadata';
import { Observable, throwError } from 'rxjs';
import { catchError, finalize, map } from 'rxjs/operators';
import { IApiSettings, AppHttpClientSetting, ResponeTypes, HttpErrorHandler } from './http-client.model';
import { HttpClientStateService } from './http-client-state.service';

@Injectable()
export abstract class HttpAbstractService {
	constructor(
		public httpClient: HttpClient,
		public setting: AppHttpClientSetting,
		private _httpState: HttpClientStateService,
		@Optional() private _httpErrorHandler: HttpErrorHandler
	) {}

	public responseInterceptor(observableRes: Observable<any>, settings: IApiSettings): Observable<any> {
		return observableRes.pipe(
			map(res => this._baseMapper(res, settings)),
			finalize(() => {
				this._onFinalize();
			}),
			catchError((error, _) => {
				this._httpState.resetState();

				if (this._httpErrorHandler && (settings.handleError == null || settings.handleError)) {
					return this._httpErrorHandler.handleError(error);
				} else {
					return throwError(error);
				}
			})
		);
	}

	private _baseMapper(res: HttpResponse<any>, apiSettings: IApiSettings): any {
		if (res.status === HttpStatus.OK) {
			switch (apiSettings.responeType) {
				case ResponeTypes.Json:
				case ResponeTypes.Text: {
					if (apiSettings.dataMapper) {
						return apiSettings.dataMapper(res.body);
					}

					return res.body;
				}
				case ResponeTypes.Blob: {
					// TODO
				}
			}
		}
	}

	private _onFinalize() {
		// TODO
	}
}
