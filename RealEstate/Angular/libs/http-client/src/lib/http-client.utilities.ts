// https://github.com/netmedia/angular-architecture-patterns

import { HttpParams } from '@angular/common/http';
import 'reflect-metadata';
import { HttpAbstractService } from './http-client.abstract-service';
import { IApiSettings, ResponeTypes } from './http-client.model';
import { BodyMetadataKey, ParamObject, PathMetadataKey, QueryMetadataKey } from './http-client.model.internal';

export function methodBuilder(method: 'DELETE' | 'GET' | 'HEAD' | 'JSONP' | 'OPTIONS' | 'PATCH' | 'POST' | 'PUT') {
	return function(url: string): MethodDecorator {
		return function(target: HttpAbstractService, propertyKey: string, descriptor: any) {
			const pathParams = Reflect.getOwnMetadata(PathMetadataKey, target, propertyKey);
			const queryParams = Reflect.getOwnMetadata(QueryMetadataKey, target, propertyKey);
			const bodyParams = Reflect.getOwnMetadata(BodyMetadataKey, target, propertyKey);

			descriptor.value = function(...args: any[]) {
				const _this = this as HttpAbstractService;
				const _body = createBody(bodyParams, args);
				const _path = createPath(pathParams, url, args);
				const _params = createParams(queryParams, args);
				const _headers = _this.setting.defaultHeader;
				const _apiSetttings = getApiSettings(descriptor.apiSettings);
				const _targetUrl = getTargetUrl(_this.setting.baseUrl, _path, _params != null);

				let observable = _this.httpClient.request(method, _targetUrl, {
					body: _body,
					headers: _headers,
					params: _params,
					reportProgress: false,
					observe: 'response', // only observe http response result
					responseType: _apiSetttings.responeType
				});

				// intercept the response
				observable = _this.responseInterceptor(observable, _apiSetttings);

				return observable;
			};

			return descriptor;
		};
	};
}

export function paramBuilder(metadataKey: Symbol) {
	return function(key: string): ParameterDecorator {
		return function(target: HttpAbstractService, propertyKey: string, parameterIndex: number) {
			const existingParameters: ParamObject[] = Reflect.getOwnMetadata(metadataKey, target, propertyKey) || [];

			existingParameters.push({
				key: key,
				parameterIndex: parameterIndex
			});

			Reflect.defineMetadata(metadataKey, existingParameters, target, propertyKey);
		};
	};
}

function createBody(bodyParams: ParamObject[], args: any[]): any {
	return bodyParams ? args[bodyParams[0].parameterIndex] : null;
}

function createPath(pathParams: ParamObject[], url: string, args: any[]): string {
	let resUrl: string = url;

	if (pathParams) {
		for (const k in pathParams) {
			if (pathParams.hasOwnProperty(k)) {
				resUrl = resUrl.replace(`{${pathParams[k].key}}`, args[pathParams[k].parameterIndex]);
			}
		}
	}

	return resUrl;
}

function createParams(queryParams: ParamObject[], args: any[]): HttpParams {
	let params = null;

	if (queryParams) {
		params = new HttpParams();

		queryParams
			.filter(p => args[p.parameterIndex]) // filter out optional parameters
			.forEach(p => {
				const key = p.key;
				const value = args[p.parameterIndex];

				// if value is object, append each key of object to query parameters
				if (value instanceof Object) {
					for (const valueKey in value) {
						if (value.hasOwnProperty(valueKey)) {
							params = params.append(valueKey, value[valueKey]);
						}
					}
				} else {
					params = params.append(key, value);
				}
			});
	}

	return params;
}

function getTargetUrl(baseUrl: string, path: string, isHaveQueryParams: boolean) {
	return `${baseUrl}/${path}${isHaveQueryParams ? '/' : ''}`;
}

function getApiSettings(apiSettings?: IApiSettings): IApiSettings {
	const defaultSettings: IApiSettings = {
		responeType: ResponeTypes.Json
	};

	return apiSettings ? { ...defaultSettings, ...apiSettings } : defaultSettings;
}
