﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using RealEstate.Common.Constants;
using System.IO;

namespace RealEstate.Middlewares
{
    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class AngularAppMiddlewareExtensions
    {
        public static IApplicationBuilder UseAngularApp(this IApplicationBuilder builder)
        {
            builder.Map(AppSettings.AdminAppRoute, (appBuilder) =>
            {
                appBuilder.HandleAngularMapRoute(AppSettings.AdminAppName);
            });

            builder.Map(AppSettings.ClientAppRoute, (appBuilder) =>
            {
                appBuilder.HandleAngularMapRoute(AppSettings.ClientAppName);
            });

            return builder;
        }

        private static void HandleAngularMapRoute(
            this IApplicationBuilder builder,
            string appName
        )
        {
            var env = (IHostingEnvironment)builder.ApplicationServices.GetService(typeof(IHostingEnvironment));
			var path = Path.Combine(Directory.GetCurrentDirectory(), $"{AppSettings.AngularRootPath}/{AppSettings.AngularDistPath}/{appName}");

			if (!Directory.Exists(path)) Directory.CreateDirectory(path);

			var staticFileOption = new StaticFileOptions
			{
				FileProvider = new PhysicalFileProvider(path)
			};

			if (env.IsProduction())
			{
				builder.UseSpaStaticFiles(staticFileOption);
			}

			builder.UseSpa(spa =>
            {
				spa.Options.SourcePath = AppSettings.AngularRootPath;
				spa.Options.DefaultPageStaticFileOptions = staticFileOption;

                if (env.IsDevelopment())
                {
					switch (appName)
                    {
                        case AppSettings.ClientAppName:
                            {
                                spa.UseAngularCliServer(npmScript: "start:client");
                                break;
                            }
                        case AppSettings.AdminAppName:
                            {
                                spa.UseAngularCliServer(npmScript: "start:admin");
                                break;
                            }
                    }
                }
            });
        }
    }
}
