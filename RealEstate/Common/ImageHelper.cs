﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace RealEstate.Common
{
    public static class ImageHelper
    {
        public static Size ResizeKeepAspect(this Size src, int maxWidth = 1024, int maxHeight = 768, bool enlarge = false)
        {
            maxWidth = enlarge ? maxWidth : Math.Min(maxWidth, src.Width);
            maxHeight = enlarge ? maxHeight : Math.Min(maxHeight, src.Height);

            decimal rnd = Math.Min(maxWidth / (decimal)src.Width, maxHeight / (decimal)src.Height);
            return new Size((int)Math.Round(src.Width * rnd), (int)Math.Round(src.Height * rnd));
        }

        public static Size ResizeThumbnails(this Size src)
        {

            return ResizeKeepAspect(src, 640, 360);
        }
    }
}
