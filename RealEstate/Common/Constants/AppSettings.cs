﻿namespace RealEstate.Common.Constants
{
    public static class AppSettings
    {
        public const string ClientAppRoute = "";
        public const string AdminAppRoute = "/admin";

        public const string AngularRootPath = "Angular";
        public const string AngularDistPath = "dist/apps";

        public const string ClientAppName = "client-app";
        public const string AdminAppName = "admin-app";
    }
}
