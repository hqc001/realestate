﻿using Microsoft.Extensions.Options;
using RealEstate.Contracts;
using RealEstate.Models;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace RealEstate.Services
{
	public class EmailSender : IEmailSender
	{
		private readonly SmtpClient _smtpClient;
		private readonly EmailSenderConfig _emailSenderConfig;

		public EmailSender(IOptions<EmailSenderConfig> emailSenderConfig)
		{
			_emailSenderConfig = emailSenderConfig.Value;
			_smtpClient = new SmtpClient
			{
				Host = _emailSenderConfig.Host,
				UseDefaultCredentials = false,
				Credentials = new NetworkCredential(_emailSenderConfig.UserName, _emailSenderConfig.Password),
				DeliveryFormat = SmtpDeliveryFormat.International,
				DeliveryMethod = SmtpDeliveryMethod.Network,
				EnableSsl = true,
				Port = _emailSenderConfig.Port
			};
		}

		public Task SendAsync(MailMessage mailMessage)
		{
			mailMessage.From = new MailAddress(_emailSenderConfig.UserName);
			mailMessage.To.Add(_emailSenderConfig.MailTo);

			_smtpClient.SendAsync(mailMessage, null);

			return Task.CompletedTask;
		}
	}
}
