﻿using Microsoft.Extensions.Logging;
using RealEstate.Contracts;
using RealEstate.Models;
using System;
using System.Net.Mail;
using System.Threading.Tasks;

namespace RealEstate.Services
{
	public class CustomerRequestService : ICustomerRequestService
	{
		private readonly IEmailSender _emailSender;
        private readonly IViewRenderService _viewService;
        private readonly ILogger<CustomerRequestService> _logger;

        public CustomerRequestService(IEmailSender emailSender, IViewRenderService viewService, ILogger<CustomerRequestService> logger)
		{
			_emailSender = emailSender;
            _viewService = viewService;
            _logger = logger;
        }

		public async Task SendRequestAsync(CustomerRequestFormDto customerRequestFormDto)
		{
			var emailMessage = new MailMessage { 
				Subject = $"[canhosg24h][Hỏi thêm thông tin] Khách hàng {customerRequestFormDto.Firstname} {customerRequestFormDto.Lastname} {customerRequestFormDto.Phone}",
				Body = await _viewService.RenderToStringAsync("Email/RequestInfoTemplate.cshtml", customerRequestFormDto),
                IsBodyHtml = true
            };
            try
            {
                await _emailSender.SendAsync(emailMessage);
            }
			catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
		}
	}
}
