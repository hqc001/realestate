﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RealEstate.Core.Contracts.Services;
using RealEstate.Core.Dtos.Commons;
using RealEstate.Core.Dtos.Invesloper;
using RealEstate.Core.Dtos.Table;
using System.Threading.Tasks;

namespace RealEstate.Controllers.Admin
{
	[Route("admin/api/invesloper")]
	[Authorize(Roles = "Admin")]
	public class InvesloperApiController : Controller
	{
		private readonly IInvesloperService _invesloperService;

		public InvesloperApiController(IInvesloperService invesloperService)
		{
			_invesloperService = invesloperService;
		}

		[HttpGet("list")]
		public async Task<IActionResult> GetInveslopers(TableParameterDto tableParams)
		{
			var inveslopers = await _invesloperService.GetInveslopersAsync(tableParams);
			return Ok(inveslopers);
		}

		[HttpGet]
		public async Task<IActionResult> GetInvesloperDetails(int id)
		{
			var invDetails = await _invesloperService.GetInvesloperDetailsAsync(id);
			return Ok(invDetails);
		}

		[HttpPost]
		public async Task<IActionResult> CreateInvesloper([FromBody] InvesloperDetailsDto invesloperDetails)
		{
			var result = await _invesloperService.CreateOrUpdateInvesloperAsync(invesloperDetails);
			return Ok(result);
		}

		[HttpPut]
		public async Task<IActionResult> UpdateInvesloper([FromBody] InvesloperDetailsDto invesloperDetails)
		{
			var result = await _invesloperService.CreateOrUpdateInvesloperAsync(invesloperDetails);
			return Ok(result);
		}

		[HttpDelete]
		public async Task<IActionResult> DeleteInvesloper(int id)
		{
			var result = await _invesloperService.DeleteInvesloperAsync(id);

			if (result.DeleteResult == ServiceActionResult.Successed)
			{
				return Ok(result.Message);
			}

			if (result.DeleteResult == ServiceActionResult.NotFound)
			{
				return NotFound(result.Message);
			}

			return BadRequest(result.Message);
		}
	}
}
