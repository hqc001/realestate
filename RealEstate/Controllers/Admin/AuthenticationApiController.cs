﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using RealEstate.Core.Dtos;
using RealEstate.Core.Identity;
using System.Threading.Tasks;

namespace RealEstate.Controllers.Admin
{
	[Route("admin/api/authentication")]
	[Authorize(Roles = "Admin")]
	public class AuthenticationApiController : Controller
	{
		private readonly SignInManager<User> _signInManager;
		private readonly UserManager<User> _userManager;

		public AuthenticationApiController(SignInManager<User> signInManager, UserManager<User> userManager)
		{
			_signInManager = signInManager;
			_userManager = userManager;
		}

		[AllowAnonymous]
		[HttpPost("login")]
		public async Task<IActionResult> Login([FromBody] LoginFormDto loginFormDto)
		{
			var signInResult = await _signInManager.PasswordSignInAsync(
				loginFormDto.UserName,
				loginFormDto.Password,
				isPersistent: loginFormDto.Remember,
				lockoutOnFailure: false
			);

			return HandleSignInResult();

			IActionResult HandleSignInResult()
			{
				if (signInResult.Succeeded)
				{
					return Ok();
				}

				if (signInResult.IsNotAllowed)
				{
					return Unauthorized();
				}

				if (signInResult.IsLockedOut)
				{
					return BadRequest();
				}

				return BadRequest();
			}
		}
	
		[HttpPost("logout")]
		public async Task LogOut() {
			await _signInManager.SignOutAsync();
		}

		[HttpGet]
		public async Task<IActionResult> GetUserInfo()
		{
			var user = await _userManager.GetUserAsync(User);
			var roles = await _userManager.GetRolesAsync(user);

			return Ok(new {
				user.UserName,
				Roles = roles
			});
		}
	}
}
