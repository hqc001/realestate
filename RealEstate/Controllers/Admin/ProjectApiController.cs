﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RealEstate.Core.Contracts.Services;
using RealEstate.Core.Dtos.Commons;
using RealEstate.Core.Dtos.Project;
using RealEstate.Core.Dtos.Table;
using System.Threading.Tasks;

namespace RealEstate.Controllers.Admin
{
	[Route("admin/api/project")]
    [Authorize(Roles = "Admin")]
    public class ProjectApiController : Controller
	{
		private readonly IProjectService projectService;

		public ProjectApiController(IProjectService _projectService)
		{
			projectService = _projectService;
		}

		[HttpGet("list")]
		public async Task<IActionResult> GetAll()
		{
			var projects = await projectService.GetProjectsAsync();

			return Ok(projects);
		}

		[HttpGet("form")]
		public async Task<IActionResult> GetProjectFormData([FromQuery] int id)
		{
			var formData = await projectService.GetProjectFormData(id);
			return Ok(formData);
		}

		[HttpGet("table")]
		public async Task<IActionResult> GetProjectTableData(TableParameterDto tableParams)
		{
			var tableData = await projectService.GetProjectTableData(tableParams);
			return Ok(tableData);
		}

		[HttpPost]
		public async Task<IActionResult> CreateProject([FromBody] ProjectDto project)
		{
			var rs = await projectService.CreateOrUpdateProject(project);
			return HandleCreateUpdateResult(rs);
		}

		[HttpPut]
		public async Task<IActionResult> UpdateProject([FromBody] ProjectDto project)
		{
			var rs = await projectService.CreateOrUpdateProject(project);
			return HandleCreateUpdateResult(rs);
		}

		[HttpDelete]
		public async Task<IActionResult> DeleteProject(int id)
		{
			var rs = await projectService.DeleteProjectAsync(id);

			if (rs.DeleteResult == ServiceActionResult.Successed)
			{
				return Ok(rs.Message);
			}

			if (rs.DeleteResult == ServiceActionResult.NotFound)
			{
				return NotFound(rs.Message);
			}

			return BadRequest(rs.Message);
		}

		private IActionResult HandleCreateUpdateResult(ProjectCreateUpdateResultDto createUpdateResult) {
			switch (createUpdateResult.CreateUpdateResult)
			{
				case ServiceActionResult.Successed:
					{
						return Ok(new { createUpdateResult.Id, createUpdateResult.Message });
					}

				case ServiceActionResult.NotFound:
					{
						return NotFound(createUpdateResult.Message);
					}
				case ServiceActionResult.Failed:
				default:
					{
						return BadRequest(createUpdateResult.Message);
					}
			}
		}
	}
}