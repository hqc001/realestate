﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RealEstate.Core.Contracts.Services;
using RealEstate.Core.Dtos.Commons;
using RealEstate.Core.Dtos.Image;
using RealEstate.Core.Dtos.Table;
using System.IO;
using System.Threading.Tasks;

namespace RealEstate.Controllers.Admin
{
    [Route("admin/api/image")]
    [Authorize(Roles = "Admin")]
    public class ImageApiController : Controller
    {
        private readonly IImageService _service;
        public ImageApiController(IImageService service)
        {
            _service = service;
        }

        [HttpGet("table")]
        public async Task<IActionResult> GetImageTableData(TableParameterDto tableParams)
        {
            var tableData = await _service.GetImageTableDataAsync(tableParams);
            return Ok(tableData);
        }

        [HttpDelete]
        public async Task<IActionResult> DeletePicture(ImageDto image)
        {
            var rs = await _service.DeleteImageAsync(image.Id);

            if (rs == ServiceActionResult.Successed)
            {
                _deletefile(image.Name);
                _deletefile(image.ThumbnailName);

                void _deletefile(string fileName)
                {
                    var rootFolder = "wwwroot/Upload";

                    if (System.IO.File.Exists(Path.Combine(rootFolder, fileName)))
                    {
                        // If file found, delete it    
                        System.IO.File.Delete(Path.Combine(rootFolder, fileName));
                    }
                }

                return Ok("Xóa thành Công");
            }

            if (rs == ServiceActionResult.NotFound)
            {
                return NotFound("Không tìm thấy hình ảnh");
            }

            return BadRequest("Xóa thất bại");
        }
    }
}