﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RealEstate.Common;
using RealEstate.Core.Contracts.Services;
using RealEstate.Core.Dtos.Commons;
using RealEstate.Core.Dtos.Image;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace RealEstate.Controllers.Admin
{
    [Route("admin/api/upload")]
    [Authorize(Roles = "Admin")]
    public class UploadApiController : Controller
    {
        private readonly IImageService _imageService;
        public UploadApiController(IImageService imageService)
        {
            _imageService = imageService;
        }

        [HttpPost("files")]
        public async Task<ActionResult> FilesAsync()
        {
            try
            {
                var projectId = int.Parse(Request.Form["projectId"].ToString());
                var imageCount = await _imageService.GetImageCount();
                var list = new List<ImageDto>();

                ImageDto image;
                foreach (var file in Request.Form.Files)
                {
                    image = new ImageDto();

                    using (var data = new MemoryStream())
                    {
                        file.CopyTo(data);
                        data.Seek(0, SeekOrigin.Begin);

                        var buf = new byte[data.Length];
                        data.Read(buf, 0, buf.Length);
                        using (Image a = Image.FromStream(data))
                        {
                            Bitmap b = new Bitmap(a, ImageHelper.ResizeThumbnails(a.Size));
                            string folderName = "wwwroot/Upload";

                            if (!Directory.Exists(folderName))
                            {
                                Directory.CreateDirectory(folderName);
                            }

                            if (file.Length > 0)
                            {
                                imageCount = imageCount + 1;
                                string fileName = $"{imageCount}.jpg";
                                string fullPath = Path.Combine(folderName, fileName);
                                using (var stream = new FileStream(fullPath, FileMode.Create))
                                {
                                    file.CopyTo(stream);
                                }

                                var thumbnailName = $"thumbnail-{fileName}";
                                b.Save(Path.Combine(folderName, thumbnailName), ImageFormat.Jpeg);

                                b.Dispose();
                                image.Name = fileName;
                                image.ThumbnailName = thumbnailName;
                                image.ProjectId = projectId;
                            };
                        }
                    }
                    list.Add(image);

                    
                }
                var result = await _imageService.CreateImage(list);

                if (result == ServiceActionResult.Successed)
                {
                    return Json("Upload Successful.");
                }

                return Json("Upload fail.");
            }
            catch (System.Exception ex)
            {
                return Json("Upload Failed: " + ex.Message);
            }
        }
    }
}