﻿using Microsoft.AspNetCore.Mvc;
using RealEstate.Core.Contracts.Services;
using System.Threading.Tasks;

namespace RealEstate.Controllers.Client
{
    [Route("api/project")]
    public class ProjectApiController : ControllerBase
    {
        private readonly IProjectService _projectService;

        public ProjectApiController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet]
        public async Task<IActionResult> GetProjects()
        {
            var data = await _projectService.GetHomePageProjects();
            return Ok(data);
        }

        [HttpGet("detail")]
        public async Task<IActionResult> GetProject(string uid)
        {
            var data = await _projectService.GetProjectAsync(uid);
            return Ok(data);
        }
    }
}
