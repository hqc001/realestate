﻿using Microsoft.AspNetCore.Mvc;
using RealEstate.Contracts;
using RealEstate.Models;
using System.Threading.Tasks;

namespace RealEstate.Controllers.Client
{
	[Route("api/customerrequest")]
	public class CustomerRequestApiController : Controller
	{
		private readonly ICustomerRequestService _customerRequestService;

		public CustomerRequestApiController(ICustomerRequestService customerRequestService)
		{
			_customerRequestService = customerRequestService;
		}

		[HttpPost]
		public async Task<IActionResult> SendRequest([FromBody] CustomerRequestFormDto customerRequestFormDto) {
			await _customerRequestService.SendRequestAsync(customerRequestFormDto);
			return Ok();
		}
	}
}
