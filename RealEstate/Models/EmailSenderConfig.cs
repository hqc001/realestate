﻿namespace RealEstate.Models
{
	public class EmailSenderConfig
	{
		public string Host { get; set; }
		public string UserName { get; set; }
		public string Password { get; set; }
		public int Port { get; set; }
		public string MailTo { get; set; }
	}
}
