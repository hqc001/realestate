﻿namespace RealEstate.Models
{
	public class CustomerRequestFormDto
	{
		public string Firstname { get; set; }
		public string Lastname { get; set; }
		public string Email { get; set; }
		public string Phone { get; set; }
		public string Message { get; set; }
	}
}
