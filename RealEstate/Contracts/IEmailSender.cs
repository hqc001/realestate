﻿using System.Net.Mail;
using System.Threading.Tasks;

namespace RealEstate.Contracts
{
	public interface IEmailSender
	{
		Task SendAsync(MailMessage mailMessage);
	}
}
