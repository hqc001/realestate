﻿using System.Threading.Tasks;

namespace RealEstate.Contracts
{
	public interface IViewRenderService
	{
		Task<string> RenderToStringAsync(string viewName, object model);
	}
}
