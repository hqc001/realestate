﻿using RealEstate.Models;
using System.Threading.Tasks;

namespace RealEstate.Contracts
{
	public interface ICustomerRequestService
	{
		Task SendRequestAsync(CustomerRequestFormDto customerRequestFormDto);
	}
}
