﻿using RealEstate.Core.Dtos.Commons;
using RealEstate.Core.Dtos.Image;
using RealEstate.Core.Dtos.Table;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RealEstate.Core.Contracts.Services
{
    public interface IImageService
    {
        Task<TableData<ImageTableDataDto>> GetImageTableDataAsync(TableParameterDto tableParameter);
        Task<ServiceActionResult> CreateImage(List<ImageDto> imagesParameter);
        Task<int> GetImageCount();
        Task<ServiceActionResult> DeleteImageAsync(int id);
    }
}
