﻿using RealEstate.Core.Dtos.Commons;
using RealEstate.Core.Dtos.Project;
using RealEstate.Core.Dtos.Table;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RealEstate.Core.Contracts.Services
{
    public interface IProjectService
	{
        // Admin function
		Task<SimpleSelectOptionDto> GetProjectsAsync();
		Task<ProjectFormDto> GetProjectFormData(int id);
		Task<TableData<ProjectTableDataDto>> GetProjectTableData(TableParameterDto tableParameter);
		Task<ProjectCreateUpdateResultDto> CreateOrUpdateProject(ProjectDto project);
		Task<ProjectDeleteResultDto> DeleteProjectAsync(int id);

        // Client function
        Task<List<HomePageProjectDto>> GetHomePageProjects();
        Task<ProjectClientDto> GetProjectAsync(string uid);
        
    }
}
