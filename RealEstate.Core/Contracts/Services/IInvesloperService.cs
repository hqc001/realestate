﻿using RealEstate.Core.Dtos.Invesloper;
using RealEstate.Core.Dtos.Table;
using System.Threading.Tasks;

namespace RealEstate.Core.Contracts.Services
{
	public interface IInvesloperService
	{
		Task<TableData<InvesloperTableDataDto>> GetInveslopersAsync(TableParameterDto tableParams);

		Task<InvesloperDetailsDto> GetInvesloperDetailsAsync(int id);

		Task<int> CreateOrUpdateInvesloperAsync(InvesloperDetailsDto invesloperDetails);

		Task<InvesloperDeleteResultDto> DeleteInvesloperAsync(int id);
	}
}
