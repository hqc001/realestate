﻿using Microsoft.AspNetCore.Identity;

namespace RealEstate.Core.Identity
{
    /*
     * Represents an EntityType user belonging to a role. 
     */
    public class UserRole : IdentityUserRole<int>
    {
    }
}
