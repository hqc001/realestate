﻿using System.ComponentModel;

namespace RealEstate.Core.Constants.Enums
{
	public enum ProjectStatuses
	{
		[Description("Đã bàn giao")]
		Closed = 1,

		[Description("Đang mở bán")]
		Opened = 2,

		[Description("Sắp mở bán")]
		ComingSoon = 3
	}
}
