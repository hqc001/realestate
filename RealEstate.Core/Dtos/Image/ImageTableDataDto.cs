﻿

namespace RealEstate.Core.Dtos.Image
{
    public class ImageTableDataDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ProjectName { get; set; }
        public string ThumbnailName { get; set; }
    }
}
