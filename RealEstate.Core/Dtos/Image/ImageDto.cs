﻿

namespace RealEstate.Core.Dtos.Image
{
    public class ImageDto
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public string Name { get; set; }
        public string ThumbnailName { get; set; }
        public string Description { get; set; }
    }
}
