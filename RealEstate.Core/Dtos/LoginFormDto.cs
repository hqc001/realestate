﻿namespace RealEstate.Core.Dtos
{
	public class LoginFormDto
	{
		public string UserName { get; set; }
		public string Password { get; set; }
		public bool Remember { get; set; }
	}
}
