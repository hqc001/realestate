﻿using RealEstate.Core.Constants.Enums;
using RealEstate.Core.Dtos.Image;
using System;

namespace RealEstate.Core.Dtos.Project
{
	public class ProjectDto
	{
		public int Id { get; set; }
		public string Uid { get; set; }
		public string Name { get; set; }
		public string Area { get; set; }
		public int Block { get; set; }
		public string City { get; set; }
		public string Code { get; set; }
		public decimal DensityOfBuilding { get; set; }
		public string District { get; set; }
		public string Floor { get; set; }
		public DateTime HandoverDate { get; set; }
		public string Penthouse { get; set; }
		public string Price { get; set; }
		public DateTime StartDate { get; set; }
		public ProjectStatuses StatusId { get; set; }
		public ProjectTypes Type { get; set; }
		public int Unit { get; set; }
		public decimal Lat { get; set; }
		public decimal Lng { get; set; }
        public string Address { get; set; }
        public int InvesloperId { get; set; }
		public string InsideUtilities { get; set; }
		public string OutsideUtilities { get; set; }
		public string Design { get; set; }
		public string Appendix { get; set; }
		public string Preferential { get; set; }
        public int? DisplayImageId { get; set; }
        public bool IsGoodPrice { get; set; }
        public string LocationDescription { get; set; }
        public string Introduction { get; set; }
    }
}
