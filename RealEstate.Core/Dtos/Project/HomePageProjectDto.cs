﻿using RealEstate.Core.Constants.Enums;
using RealEstate.Core.Dtos.Image;

namespace RealEstate.Core.Dtos.Project
{
    /// <summary>
    /// Dto for client home page project item
    /// </summary>
    public class HomePageProjectDto
    {
        public int Id { get; set; }
        public string Uid { get; set; }
        public ImageDto DisplayImage { get; set; }
        public bool IsGoodPrice { get; set; }
        public ProjectStatuses StatusId { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string District { get; set; }
        public string Price { get; set; }
        public string Area { get; set; }
    }
}
