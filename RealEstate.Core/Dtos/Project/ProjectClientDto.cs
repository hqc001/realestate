﻿using RealEstate.Core.Dtos.Image;
using RealEstate.Core.Dtos.Invesloper;
using System;
using System.Collections.Generic;
using System.Text;

namespace RealEstate.Core.Dtos.Project
{
    public class ProjectClientDto : ProjectDto
    {
        public List<ImageDto> Images { get; set; }
        public InvesloperDetailsDto Invesloper { get; set; }
    }
}
