﻿using RealEstate.Core.Constants.Enums;
using RealEstate.Core.Dtos.Commons;
using System.Collections.Generic;

namespace RealEstate.Core.Dtos.Project
{
    public class ProjectFormDto
    {
        public IEnumerable<SelectOptionDto<ProjectStatuses>> ProjectStatuses { get; set; }
        public IEnumerable<SelectOptionDto<int>> ProjectInveslopers { get; set; }
        public IEnumerable<SelectOptionDto<ProjectTypes>> ProjectTypes { get; set; }
        public IEnumerable<SelectOptionDto<int>> ProjectImages { get; set; }

        public ProjectDto Project { get; set; }
    }
}
