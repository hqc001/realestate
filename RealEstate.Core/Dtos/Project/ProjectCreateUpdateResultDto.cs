﻿using RealEstate.Core.Dtos.Commons;

namespace RealEstate.Core.Dtos.Project
{
	public class ProjectCreateUpdateResultDto
	{
		public int? Id { get; set; }
		public string Message { get; set; }
		public ServiceActionResult CreateUpdateResult { get; set; }
	}
}
