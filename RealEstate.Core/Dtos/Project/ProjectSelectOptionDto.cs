﻿using RealEstate.Core.Dtos.Commons;
using System;
using System.Collections.Generic;
using System.Text;

namespace RealEstate.Core.Dtos.Project
{
    public class ProjectSelectOptionDto<TKey>: SelectOptionDto<TKey>
    {
        public int ImageCount { get; set; }
    }
}
