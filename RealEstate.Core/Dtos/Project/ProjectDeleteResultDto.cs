﻿using RealEstate.Core.Dtos.Commons;

namespace RealEstate.Core.Dtos.Project
{
	public class ProjectDeleteResultDto
	{
		public ServiceActionResult DeleteResult { get; set; }
		public string Message { get; set; }
	}
}
