﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RealEstate.Core.Dtos.Project
{
	public class ProjectTableDataDto
	{
		public int Id { get; set; }
		public string Uid { get; set; }
		public string Name { get; set; }
		public string Type { get; set; }
		public string InvesloperName { get; set; }
		public int Block { get; set; }
		public int Unit { get; set; }
	}
}
