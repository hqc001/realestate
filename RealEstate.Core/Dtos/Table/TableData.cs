﻿using System.Collections.Generic;

namespace RealEstate.Core.Dtos.Table
{
	public class TableData<T> where T: class
	{
		public int TotalRecord { get; set; }
		public IEnumerable<T> Data { get; set; }
	}
}
