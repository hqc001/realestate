﻿using System.ComponentModel.DataAnnotations;

namespace RealEstate.Core.Dtos.Table
{
	public class TableParameterDto
	{
		public string SortBy { get; set; }

		public string SortDirection { get; set; }

		[Range(0, int.MaxValue)]
		public int PageIndex { get; set; }

		[Range(1, int.MaxValue)]
		public int PageSize { get; set; }

		public string Filter { get; set; }
	}
}
