﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RealEstate.Core.Dtos
{
    public class AuditDto<TEnitityId, TUserId> : BaseDto<TEnitityId>
    {
        public TUserId CreatedBy { get; set; }
        public DateTime CreateDate { get; set; }
        public TUserId UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
