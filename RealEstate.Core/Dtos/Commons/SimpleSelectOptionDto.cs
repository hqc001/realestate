﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RealEstate.Core.Dtos.Commons
{
    public class SimpleSelectOptionDto
    {
        public IEnumerable<SelectOptionDto<int>> Items { get; set; }
    }
}
