﻿namespace RealEstate.Core.Dtos.Commons
{
	public enum ServiceActionResult
	{
		Successed = 1,
		Failed = 2,
		NotFound = 3
	}
}
