﻿namespace RealEstate.Core.Dtos.Commons
{
	public class SelectOptionDto<TValue>
	{
		public TValue Value { get; set; }
		public string Description { get; set; }
	}
}
