﻿using RealEstate.Core.Dtos.Commons;

namespace RealEstate.Core.Dtos.Invesloper
{
	public class InvesloperDeleteResultDto
	{
		public ServiceActionResult DeleteResult { get; set; }
		public string Message { get; set; }
	}
}
