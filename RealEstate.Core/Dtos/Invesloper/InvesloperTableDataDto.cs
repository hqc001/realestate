﻿namespace RealEstate.Core.Dtos.Invesloper
{
	public class InvesloperTableDataDto
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Url { get; set; }
	}
}
