﻿
namespace RealEstate.Core.Dtos
{
    public class BaseDto<Tid>
    {
        public Tid Id { get; set; }
    }
}
