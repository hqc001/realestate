﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace RealEstate.Core.Extensions
{
	public static class EnumExtension
	{
		public static string Description(this Enum value)
		{
			return value.GetType().GetMember(value.ToString()).FirstOrDefault()?.GetCustomAttribute<DescriptionAttribute>()?.Description ?? "";
		}
	}
}
